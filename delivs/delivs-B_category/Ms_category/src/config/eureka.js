const Eureka = require('eureka-js-client').Eureka;

const client = new Eureka({
  instance: {
    instanceId: 'CATEGORYSERVICE',
    app: 'CATEGORYSERVICE',
    hostName: 'delivery-ms-category',
    ipAddr: 'delivery-ms-category',
    statusPageUrl: 'http://delivery-ms-category:3007',
    port: {
      '$': 3007,
      '@enabled': 'true',
    },
    vipAddress: 'CATEGORYSERVICE',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    registerWithEureka: true,
    fetchRegistry: true,
  },
  eureka: {
    host: 'delivery-eureka-service',
    port: 8763,
    servicePath: '/eureka/apps/',
  }
});
client.logger.level('debug');
module.exports = client
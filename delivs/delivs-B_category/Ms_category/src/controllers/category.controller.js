const Category = require('../models/category.models');
const { body,check, validationResult } = require('express-validator');
exports.validate = (method) => {
    switch (method) {
      case 'create': {
       return [ 
        check('categoryName','Invalid categoryName').exists() ,

    ]   
      }
      case 'update': {
        return [ 
            check('categoryName','Invalid categoryName').exists() ,
     ]   
       }
    }
  }
// Create and Save the category
exports.create = (req, res) => {
            // Validate request
            const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

            if (!errors.isEmpty()) {
              res.status(422).json({ errors: errors.array() });
              return;
            }
    if(!req.body) {
        return res.status(400).send({
            message: "category  can not be empty"
        });
    }

    // Create a Note
    const category = new Category({
        categoryName: req.body.categoryName || null , 
    });

    // Save category in the database
    category.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the category."
        });
    });
};

// Retrieve and return all category from the database.
exports.findAll = (req, res) => {
    Category.find()
    .then(categories => {
        res.send(categories);
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving categories."
        });
    });
};

// Find a single category with a id
exports.findOne = (req, res) => {
    Category.findById(req.params.id)
    .then(category => {
        if(!category) {
            return res.status(404).send({
                message: "category not found with id " + req.params.id
            });            
        }
        res.send(category);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "category not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error retrieving category with id " + req.params.id
        });
    });
};

// Update a category identified by the id in the request
exports.update = (req, res) => {
            // Validate request
            const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

            if (!errors.isEmpty()) {
              res.status(422).json({ errors: errors.array() });
              return;
            }
    if(!req.body) {
        return res.status(400).send({
            message: "deliveryFee content can not be empty"
        });
    }

    // Find category and update it with the request body
    Category.findByIdAndUpdate(req.params.id, {
        
        categoryName: req.body.categoryName || null , 
    }, {new: true})
    .then(category => {
        if(!category) {
            return res.status(404).send({
                message: "category not found with id " + req.params.id
            });
        }
        res.send(category);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "category not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Error updating category with id " + req.params.id
        });
    });
};

// Delete a category with the specified id in the request
exports.delete = (req, res) => {
    Category.findByIdAndRemove(req.params.id)
    .then(category => {
        if(!category) {
            return res.status(404).send({
                message: "category not found with id " + req.params.id
            });
        }
        res.send({message: "category deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "category not found with id " + req.params.id
            });                
        }
        return res.status(500).send({
            message: "Could not delete category with id " + req.params.id
        });
    });
};
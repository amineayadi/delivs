const mongoose = require('mongoose');

const CategorySchema = mongoose.Schema({
    
    categoryName: String 

} );

module.exports = mongoose.model('Category', CategorySchema);
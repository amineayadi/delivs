module.exports = (app) => {
    const category = require('../controllers/category.controller');

    // Create a new category
    app.post('/category',category.validate('create'), category.create);

    // Retrieve all categories
    app.get('/category', category.findAll);

    // Retrieveee a single category with id
    app.get('/category/:id', category.findOne);

    // Update a category with id
    app.put('/category/:id',category.validate('update'),  category.update);

    // Delete a category with id
    app.delete('/category/:id', category.delete);
}

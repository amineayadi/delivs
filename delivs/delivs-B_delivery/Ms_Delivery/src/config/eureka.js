const Eureka = require('eureka-js-client').Eureka;

const client = new Eureka({
  instance: {
    instanceId: 'DELIVERYSERVICE',
    app: 'DELIVERYSERVICE',
    hostName: 'delivery-ms-delivery',
    ipAddr: 'delivery-ms-delivery',
    statusPageUrl: 'http://delivery-ms-delivery:3002',
    port: {
      '$': 3002,
      '@enabled': 'true',
    },
    vipAddress: 'DELIVERYSERVICE',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    registerWithEureka: true,
    fetchRegistry: true,
  },
  eureka: {
    host: 'delivery-eureka-service',
    port: 8763,
    servicePath: '/eureka/apps/',
  }
});
client.logger.level('debug');
module.exports = client
const Delivery = require('../models/models.delivery');
const { body,check, validationResult } = require('express-validator');
var s3_r = require('../config/aws-reporting')
const BUCKET_R = 'delivs-reporting'
var fs = require('fs');

exports.validate = (method) => {
    switch (method) {
      case 'create': {
       return [ 
        check('startingPoint').exists().withMessage('starting point invalid'),
        check('arrivalPoint').exists().withMessage('arrival point invalid'),
        check('deliveryDate','invalid date').exists(),
        check('recipient','invalid recipient').exists().isString(),
        check('price').exists().withMessage(' price required').isNumeric().withMessage('Invalid price'),
        check('deliveryFee').exists().withMessage(' deliveryFee required').isNumeric().withMessage('Invalid deliveryFee'),
        check('total').exists().withMessage('total required').isNumeric().withMessage('Invalid total'),
        check('sender','invalid sender').optional({nullable: true}).isString(),
        check('deliveryMan','invalid deliveryman').optional({nullable: true}).isString()
    ]   
      }
      case 'update': {
        return [ 
            check('startingPoint').exists().withMessage('starting point invalid'),
            check('arrivalPoint').exists().withMessage('arrival point invalid'),
            check('deliveryDate','invalid date').exists(),
            check('recipient','invalid recipient').exists().isString(),
            check('price').exists().withMessage(' price required').isNumeric().withMessage('Invalid price'),
            check('deliveryFee').exists().withMessage(' deliveryFee required').isNumeric().withMessage('Invalid deliveryFee'),
            check('total').exists().withMessage('total required').isNumeric().withMessage('Invalid total'),
            check('sender','invalid sender').optional({nullable: true}).isString(),
            check('deliveryMan','invalid deliveryman').optional({nullable: true}).isString()
        ]   
       }
    }
  }
  exports.exportData = (req, res) => {
    const filename = 'delivs-delivery-input.csv'
    const storage = 'uploads/'
    Delivery.find().stream().pipe(Delivery.csvTransformStream())
    .pipe(fs.createWriteStream(storage+filename)).on('finish', function() {
        s3_r.putObject({
            Bucket: BUCKET_R,
            Body: fs.readFileSync(storage+filename),
            Key: filename,
            ACL:'public-read',
            Expires:new Date(2040,12,31,0,0,0,0)
          })
            .promise()
            .then(response => { res.send(`${s3_r.getSignedUrl('getObject', { Bucket: BUCKET_R, Key: filename })}`.split("?").shift().toString())
            })
            .catch(err => {
                res.send(false)
              console.log('failed:', err)
            })
    })

   

};
// Create and Save a new Delivery
exports.create = (req, res) => {
            // Validate request
            const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

            if (!errors.isEmpty()) {
              res.status(422).json({ errors: errors.array() });
              return;
            }
    if(!req.body) {
        return res.status(400).send({
            message: "Delivery can not be empty"
        });
    }
    // Create a delivery
    const delivery = new Delivery({
        sender:req.body.sender || null,
        startingPoint: req.body.startingPoint|| null,
        startingcoordinates: req.body.startingcoordinates|| null,
        arrivalPoint: req.body.arrivalPoint|| null,
        zone: req.body.zone || "Autres",
        arrivalcoordinates: req.body.arrivalcoordinates|| null,
        deliveryDate:req.body.deliveryDate|| new Date(),
        recipient: req.body.recipient|| null,
        object: req.body.object || null,
        description: req.body.description || null,
        deliveryMan: req.body.deliveryMan ||null,
	    estimatedDuration:req.body.estimatedDuration || null,
        status:req.body.status|| null,
        statusHistory:req.body.statusHistory|| null,
        distance:req.body.distance|| null,
        price:req.body.price || 0,
        deliveryFee:req.body.deliveryFee || 0,
        delivsFee:req.body.delivsFee || 0,
        total:req.body.total|| 0,
        payment:req.body.payment || null,
        reviewed:req.body.reviewed || false
    });

    // Save Delivery in the database
    delivery.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Delivery."
        });
    });
};


// Retrieve and return all deliveries from the database.
exports.findAll = (req, res) => {
    Delivery.find().sort({deliveryDate:-1})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving deliveries."
        });
    });
};
// Retrieve and return all deliveries beloning to a sender
exports.findBySender = (req, res) => {
    Delivery.find({sender:req.params.idSender}).sort({deliveryDate:-1})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving deliveries."
        });
    });
};
// Retrieve and return all deliveries beloning to a delivery man
exports.findByDeliveryMan = (req, res) => {
    Delivery.find({deliveryMan:req.params.idDeliveryMan}).sort({deliveryDate:-1})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving deliveries."
        });
    });
};
// Retrieve and return all deliveries beloning to a delivery man and status
exports.findByStatusAndDeliveryMan = (req, res) => {
    Delivery.find({deliveryMan:req.params.idDeliveryMan,status: { $in: req.params.status.split(',') }}).sort({deliveryDate:-1})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving deliveries."
        });
    });
};
// Retrieve and return all deliveries beloning to a sender and status
exports.findByStatusAndSender = (req, res) => {
    Delivery.find({sender:req.params.idSender,status: { $in: req.params.status.split(',') }}).sort({deliveryDate:-1})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving deliveries."
        });
    });
};
// Retrieve and return all deliveries beloning to a recipient and status
exports.findByStatusAndRecipient = (req, res) => {
    Delivery.find({recipient:req.params.idRecipient,status: { $in: req.params.status.split(',') }}).sort({deliveryDate:-1})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving deliveries."
        });
    });
};
// Retrieve and return all deliveries beloning to a recipient 
exports.findByRecipient = (req, res) => {
    Delivery.find({recipient:req.params.idRecipient}).sort({deliveryDate:-1})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving deliveries."
        });
    });
};
// Retrieve and return all deliveries by status
exports.findByStatus = (req, res) => {
    Delivery.find({status:req.params.status}).sort({deliveryDate:-1})
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving deliveries."
        });
    });
};
// Find a single Delivery with an id
exports.findOne = (req, res) => {
    Delivery.findById(req.params.deliveryId)
    .then(delivery => {
        if(!delivery) {
            return res.status(404).send({
                message: "Delievry not found with id " + req.params.deliveryId
            });            
        }
        res.send(delivery);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Delivery not found with id " + req.params.deliveryId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Delivery with id " + req.params.deliveryId
        });
    });
};

// Update a Delivery identified by the id in the request
exports.update = (req, res) => {
        // Validate request
        const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

        if (!errors.isEmpty()) {
          res.status(422).json({ errors: errors.array() });
          return;
        }
    if(!req.body) {
        return res.status(400).send({
            message: "can not be empty"
        });
    }

    // Find Delivery and update it with the request body
    Delivery.findByIdAndUpdate(req.params.deliveryId, {

        sender:req.body.sender || null,
        startingPoint: req.body.startingPoint|| null,
        arrivalPoint: req.body.arrivalPoint|| null,
        zone: req.body.zone || "Autres",
        deliveryDate:req.body.deliveryDate|| null,
        recipient: req.body.recipient|| null,
        object: req.body.object || null,
        description: req.body.description || null,
        deliveryMan: req.body.deliveryMan ||null,
	    estimatedDuration:req.body.estimatedDuration || null,
        status:req.body.status|| null,
        statusHistory:req.body.statusHistory|| null,
        distance:req.body.distance|| null,
        price:req.body.price || null,
        deliveryFee:req.body.deliveryFee || null,
        delivsFee:req.body.delivsFee || null,
        total:req.body.total|| null,
        payment:req.body.payment || null,
        reviewed:req.body.reviewed || false
    }, {new: true})
    .then(delivery => {
        if(!delivery) {
            return res.status(404).send({
                message: "Delivery not found with id " + req.params.deliveryId
            });
        }
        res.send(delivery);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Delivery not found with id " + req.params.deliveryId
            });                
        }
        return res.status(500).send({
            message: "Error updating Delivery with id " + req.params.deliveryId
        });
    });
};


// Delete a Delivery with the specified Id in the request
exports.delete = (req, res) => {
    Delivery.findByIdAndRemove(req.params.deliveryId)
    .then(delivery => {
        if(!delivery) {
            return res.status(404).send({
                message: "Delivery not found with id " + req.params.deliveryId
            });
        }
        res.send({message: "Delivery deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Delivery not found with id " + req.params.deliveryId
            });                
        }
        return res.status(500).send({
            message: "Could not delete Delivery with id " + req.params.deliveryId
        });
    });
};
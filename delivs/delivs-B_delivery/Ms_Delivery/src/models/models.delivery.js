const mongoose = require('mongoose');
var mongooseToCsv = require('mongoose-to-csv');

const DeliverySchema = mongoose.Schema({

	sender: String,
	startingPoint: String,
	startingcoordinates:{
			latitude:String,
			longitude:String
	},
	arrivalPoint: String,
	zone:String,
	arrivalcoordinates:{
			latitude:String,
			longitude:String
	},
	deliveryDate:{ type: Date, default: Date.now },
	recipient: String,
	object: String,
	description: String,
	deliveryMan: String,
	estimatedDuration:Number,
	startTime:Date,
	arrivalTime:Date,
	status:   String,
	statusHistory:[{
		status:String
	}],
	distance: Number,
	payment:String,
	price:Number,
	deliveryFee:Number,
	delivsFee:Number,
	total: Number,
	reviewed:Boolean
	
}, {
    timestamps: true
});
DeliverySchema.plugin(mongooseToCsv, {
	delimiter : ';',
	headers: '_id sender recipient object description deliveryMan status distance payment price deliveryFee delivsFee total reviewed startingPoint startingcoordinates_latitude startingcoordinates_longitude arrivalPoint zone arrivalcoordinates_latitude arrivalcoordinates_longitude createdAt updatedAt',
	virtuals: {
		'startingcoordinates_latitude': function(doc) {
			return doc.startingcoordinates.latitude;
		  },
		  'startingcoordinates_longitude': function(doc) {
			  return doc.startingcoordinates.longitude;
		  },
		  'arrivalcoordinates_latitude': function(doc) {
			return doc.arrivalcoordinates.latitude;
		  },
		  'arrivalcoordinates_longitude': function(doc) {
			  return doc.arrivalcoordinates.longitude;
		  },
		  'createdAt': function(doc) {
			return new Date(doc.createdAt).getDate()+'/'+(new Date(doc.createdAt).getMonth()+1)+'/'+new Date(doc.createdAt).getFullYear();
		},
		'updatedAt': function(doc) {
			return new Date(doc.updatedAt).getDate()+'/'+(new Date(doc.updatedAt).getMonth()+1)+'/'+new Date(doc.updatedAt).getFullYear();
	  }
		
	  }
	
  });
module.exports = mongoose.model('Delivery', DeliverySchema);
module.exports = (app) => {
    const delivery = require('../controllers/controllers.delivery');
 // export data CSV
 app.get('/exportData', delivery.exportData);
    // Create a new delivery 
    app.post('/delivery', delivery.validate('create'),delivery.create);

    // Retrieve all deliveries
    app.get('/delivery', delivery.findAll);

    // Retrieve all deleveries belonging to a sender
    app.get('/deliveryBySender/:idSender', delivery.findBySender);

    // Retrieve all deleveries belonging to a deliveryMan
    app.get('/deliveryByDeliveryMan/:idDeliveryMan', delivery.findByDeliveryMan);

    // Retrieve all deleveries belonging to a recipient
    app.get('/deliveryByRecipient/:idRecipient', delivery.findByRecipient);

    // Retrieve all deleveries belonging to a status 
    app.get('/deliveryByStatus/:status', delivery.findByStatus);

  // Retrieve all deleveries belonging to a status and  deliveryMan
  app.get('/deliveryByStatusAndDeliveryman/:idDeliveryMan/:status', delivery.findByStatusAndDeliveryMan);

   // Retrieve all deleveries belonging to a status and  a sender
   app.get('/deliveryByStatusAndSender/:idSender/:status', delivery.findByStatusAndSender);
 // Retrieve all deleveries belonging to a status and  a recipient
 app.get('/deliveryByStatusAndRecipient/:idRecipient/:status', delivery.findByStatusAndRecipient);
    // Retrieve a single delivery with id
    app.get('/delivery/:deliveryId', delivery.findOne);

    // Update a delivery with id
    app.put('/delivery/:deliveryId', delivery.update);

    // Delete a delivery with id
    app.delete('/delivery/:deliveryId', delivery.delete);
}
var ip = require('./env.js');
const Eureka = require('eureka-js-client').Eureka;

const client = new Eureka({
  instance: {
    instanceId: 'DELIVERYFEESERVICE',
    app: 'DELIVERYFEESERVICE',
    hostName: 'delivery-ms-deliveryfee',
    ipAddr: 'delivery-ms-deliveryfee',
    statusPageUrl: 'http://delivery-ms-deliveryfee:3006',
    port: {
      '$': 3006,
      '@enabled': 'true',
    },
    vipAddress: 'DELIVERYFEESERVICE',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    registerWithEureka: true,
    fetchRegistry: true,
  },
  eureka: {
    host: 'delivery-eureka-service',
    port: 8763,
    servicePath: '/eureka/apps/',
  }
});
client.logger.level('debug');
module.exports = client
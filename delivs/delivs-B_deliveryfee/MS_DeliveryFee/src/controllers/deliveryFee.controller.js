const DeliveryFee = require('../models/deliveryFee');
const { body,check, validationResult } = require('express-validator');
exports.validate = (method) => {
    switch (method) {
      case 'create': {
       return [ 
        check('priceKm','Invalid priceKm').exists() ,
        check('limit','Invalid limit').exists() ,
        check('profitPercentage','Invalid feePercentage').exists() ,

    ]   
      }
      case 'update': {
        return [ 
            check('priceKm','Invalid priceKm').exists() ,
            check('limit','Invalid limit').exists() ,
            check('profitPercentage','Invalid feePercentage').exists() ,
     ]   
       }
    }
  }
// Create and Save the delivery Fee
exports.create = (req, res) => {
            // Validate request
            const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

            if (!errors.isEmpty()) {
              res.status(422).json({ errors: errors.array() });
              return;
            }
    if(!req.body) {
        return res.status(400).send({
            message: "delivery Fee  can not be empty"
        });
    }

    // Create a Note
    const deliveryFee = new DeliveryFee({
        priceKm: req.body.priceKm || 0 , 
        limit: req.body.limit || 0 , 
        feeDT: req.body.feeDT || 0 ,
        orderLimit: req.body.orderLimit || 0 ,
        profitPercentage: req.body.profitPercentage || 0 ,
        deliveryLimit: req.body.deliveryLimit || 0  
    });

    // Save delivery Fee in the database
    deliveryFee.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the deliveryFee."
        });
    });
};

// Retrieve and return all deliveryFee from the database.
exports.findAll = (req, res) => {
    DeliveryFee.find()
    .then(deliveryFees => {
        res.send(deliveryFees);
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving DeliveryFees."
        });
    });
};
// Retrieve and return  deliveryFee count from the database.
exports.findLength = (req, res) => {
    DeliveryFee.countDocuments()
    .then(deliveryFees => {
        res.send(deliveryFees.toString());
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving DeliveryFees."
        });
    });
};
// Retrieve and return the deliveryFee  from the database.
exports.find = (req, res) => {
    DeliveryFee.find()
    .then(deliveryFees => {
        res.send(deliveryFees[0]);
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving DeliveryFees."
        });
    });
};
// Find a single delivery Fee with a id
exports.findOne = (req, res) => {
    DeliveryFee.findById(req.params.feeId)
    .then(deliveryFee => {
        if(!deliveryFee) {
            return res.status(404).send({
                message: "deliveryFee not found with id " + req.params.feeId
            });            
        }
        res.send(deliveryFee);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "deliveryFee not found with id " + req.params.feeId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving deliveryFee with id " + req.params.feeId
        });
    });
};

// Update a deliveryFee identified by the feeId in the request
exports.update = (req, res) => {
            // Validate request
            const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

            if (!errors.isEmpty()) {
              res.status(422).json({ errors: errors.array() });
              return;
            }
    if(!req.body) {
        return res.status(400).send({
            message: "deliveryFee content can not be empty"
        });
    }

    // Find DeliveryFee and update it with the request body
    DeliveryFee.findByIdAndUpdate(req.params.feeId, {
        
        priceKm: req.body.priceKm || 0 , 
        limit: req.body.limit || 0 , 
        feeDT: req.body.feeDT || 0 ,
        orderLimit: req.body.orderLimit || 0 ,
        profitPercentage: req.body.profitPercentage || 0 ,
        deliveryLimit: req.body.deliveryLimit || 0 ,
        key: req.body.key || null ,   
    }, {new: true})
    .then(deliveryFee => {
        if(!deliveryFee) {
            return res.status(404).send({
                message: "DeliveryFee not found with id " + req.params.feeId
            });
        }
        res.send(deliveryFee);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "DeliveryFee not found with id " + req.params.feeId
            });                
        }
        return res.status(500).send({
            message: "Error updating DeliveryFee with id " + req.params.feeId
        });
    });
};

// Delete a DeliveryFee with the specified feeId in the request
exports.delete = (req, res) => {
    DeliveryFee.findByIdAndRemove(req.params.feeId)
    .then(deliveryFee => {
        if(!deliveryFee) {
            return res.status(404).send({
                message: "DeliveryFee not found with id " + req.params.feeId
            });
        }
        res.send({message: "DeliveryFee deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "DeliveryFee not found with id " + req.params.feeId
            });                
        }
        return res.status(500).send({
            message: "Could not delete DeliveryFee with id " + req.params.feeId
        });
    });
};
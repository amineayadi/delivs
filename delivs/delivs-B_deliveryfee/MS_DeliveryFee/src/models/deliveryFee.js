const mongoose = require('mongoose');

const DeliveryFeeSchema = mongoose.Schema({
    
    orderLimit:Number,
    priceKm:Number,
    feeDT:Number,
    profitPercentage:Number,
    limit:Number,
    deliveryLimit:Number,


    
    
    key:String

} );

module.exports = mongoose.model('DeliveryFee', DeliveryFeeSchema);
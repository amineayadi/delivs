module.exports = (app) => {
    const deliveryFee = require('../controllers/deliveryFee.controller');

    // Create a new deliveryFee
    app.post('/deliveryFee',deliveryFee.validate('create'), deliveryFee.create);

    // Retrieve all deliveryFees
    app.get('/deliveryFee', deliveryFee.findAll);
    
   // Retrieve  deliveryFee count
   app.get('/deliveryFeeCount', deliveryFee.findLength);

   // Retrieve the  single deliveryFee 
   app.get('/oneDeliveryFee', deliveryFee.find);

    // Retrieve a single deliveryFee with feeId
    app.get('/deliveryFee/:feeId', deliveryFee.findOne);

    // Update a Note with noteId
    app.put('/deliveryFee/:feeId',  deliveryFee.update);

    // Delete a Note with noteId
    app.delete('/deliveryFee/:feeId', deliveryFee.delete);
}
const Eureka = require('eureka-js-client').Eureka;

const client = new Eureka({
  instance: {
    instanceId: 'DELIVERYMANSERVICE',
    app: 'DELIVERYMANSERVICE',
    hostName: 'delivery-ms-deliveryman',
    ipAddr: 'delivery-ms-deliveryman',
    statusPageUrl: 'http://delivery-ms-deliveryman:3001',
    port: {
      '$': 3001,
      '@enabled': 'true',
    },
    vipAddress: 'DELIVERYMANSERVICE',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    registerWithEureka: true,
    fetchRegistry: true,
  },
  eureka: {
    host: 'delivery-eureka-service',
    port: 8763,
    servicePath: '/eureka/apps/',
  }
});
client.logger.level('debug');
module.exports = client
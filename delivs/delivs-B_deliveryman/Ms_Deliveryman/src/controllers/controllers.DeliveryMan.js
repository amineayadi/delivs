const DeliveryMan = require('../models/models.DeliveryMan');
const Token = require('../models/token');
const { body,check, validationResult } = require('express-validator');
var fs = require('fs');
var s3 = require('../models/aws')
const BUCKET = 'marketplaceupload'
const { smtpTransport}= require('../config/mail');
var crypto = require('crypto');
var s3_r = require('../config/aws-reporting')
const BUCKET_R = 'delivs-reporting'
exports.validate = (method) => {
    switch (method) {
      case 'create': {
       return [ 
        check('firstName').exists(),
        check('lastName').exists(),
        check('email').exists().isEmail().withMessage('Invalid email'),
        check('phoneNumber').isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid phoneNumber'),
        check('address').exists().isLength({max:100 }).withMessage('Address Too Long'),
       // check('cin').exists().isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid CIN'),
      //  check('rib').optional({nullable: true}).isNumeric().isLength({ min: 20,max:20 }).withMessage('Invalid RIB'),
        check('active').isBoolean(),
        check('password').isLength({ min: 5 }).withMessage('must be at least 5 chars long').matches(/\d/).withMessage('must contain a number'),    
    ]   
      }
      case 'update': {
        return [ 
          check('firstName').exists(),
         check('lastName').exists(),
         check('email').exists().isEmail().withMessage('Invalid email'),
         check('phoneNumber').optional({nullable: true}).isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid phoneNumber'),
         check('address').optional({nullable: true}).exists().isLength({max:100 }).withMessage('Address Too Long'),
//       check('cin').exists().isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid CIN'),
//         check('rib').optional({nullable: true}).isNumeric().isLength({ min: 20,max:20 }).withMessage('Invalid RIB'),
         check('active').isBoolean(),
         check('password').isLength({ min: 5 }).withMessage('must be at least 5 chars long').matches(/\d/).withMessage('must contain a number')  
     ]   
       }
    }
  }
  exports.exportData = (req, res) => {
    const filename = 'delivs-deliveryman-input.csv'
    const storage = 'uploads/'
    DeliveryMan.find().stream().pipe(DeliveryMan.csvTransformStream())
    .pipe(fs.createWriteStream(storage+filename)).on('finish', function() {
        s3_r.putObject({
            Bucket: BUCKET_R,
            Body: fs.readFileSync(storage+filename),
            Key: filename,
            ACL:'public-read',
            Expires:new Date(2040,12,31,0,0,0,0)
          })
            .promise()
            .then(response => { res.send(`${s3_r.getSignedUrl('getObject', { Bucket: BUCKET_R, Key: filename })}`.split("?").shift().toString())
            })
            .catch(err => {
                res.send(false)
              console.log('failed:', err)
            })
    })

   

};
  // Upload Image
exports.upload = (req, res) => {

    if(!req.body) {
        return res.status(400).send({
            message: "Body can not be empty"
        });
    }
    
    s3.putObject({
        Bucket: BUCKET,
        Body: fs.readFileSync(req.file.destination+req.file.filename),
        Key: req.file.filename,
        ACL:'public-read',
        Expires:new Date(2040,12,31,0,0,0,0)
      })
        .promise()
        .then(response => { res.send(`${s3.getSignedUrl('getObject', { Bucket: BUCKET, Key: req.file.filename })}`.split("?").shift().toString())
        })
        .catch(err => {
          console.log('failed:', err)
        })

   

};
  // Create and Save a new Note
  exports.welcomeMail = (req, res) => {
            DeliveryMan.findOne({ _id: req.body.id }).then(deliveryman=>{
                const mailOptions = {
                    from: "contact@delivs.tn",
                    to: deliveryman.email,
                    subject: "Bienvenue",
                    generateTextFromHTML: true,
                    html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
                    '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
                    '<h3 style="margin-top:5%;color:#6861e1">Bonjour '+deliveryman.firstName+'</h3>'+
                    '<p>votre compte a été confirmé par l\'adminstrateur avec succès </p>'+
                    '<p></p>'+
                    '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
                                       
                };
                smtpTransport.sendMail(mailOptions, (error, response) => {
                    if(error){res.send(false);}
                    if(response){res.send(true);}
                    smtpTransport.close();
                })
            }).catch(err=>{return res.status(400).send({ msg: 'We were unable to find a user.' });})

};
  // Create and Save a new Note
  exports.confirmMailSocial = (req, res) => {
    DeliveryMan.findOne({ _id: req.params.id }).then(deliveryman=>{
        const mailOptions = {
            from: "contact@delivs.tn",
            to: deliveryman.email,
            subject: "Bienvenue",
            generateTextFromHTML: true,
            html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
            '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
            '<h3 style="margin-top:5%;color:#6861e1">Bonjour '+deliveryman.firstName+'</h3>'+
            '</div><div style="max-width: 1000;margin:auto;" >'+
            '<p>Plutôt que de travailler derrière un bureau, exercer un job en plein air vous tente?'+
            ' Vous aimez rouler, vous êtes flexibles et aimez le contact humain? Pourquoi ne pas devenir livreur deliv’s?</p>'+
            '<p>Si vous êtes conquis à l’idée de travailler chez deliv’s vous vous demandez sans doute comment devenir livreur'+
            ' deliv’s. Nous vous expliquons la démarche à suivre pour y parvenir.</p>'+
            '<p>Ce qu’il vous faut:</p><ul><li>Être majeur et disposer d\'un statut d\'entrepreneur individuel :'+
            'Vous devez avoir plus de 18 ans et avoir le droit de travailler en Tunisie.</li>'+
            '<li>Un smartphone : iPhone iOS 10+ ou Android 5+</li>'+
            '<li>Un vélo ou un scooter:Choisissez votre mode de transport préféré pour livrer en ville</li></ul>'+
            '<p>Pour postuler à une offre d’emploi et devenir livreur deliv’s il y a plusieurs modalités à effectuer :</p>'+
            '<ul><li>Inscription sur la plateforme en ligne deliv’s: remplir un formulaire d’inscription deliv’s</li>'+
            '<li>Se procurer un équipement complet: devenir livreur deliv’s implique de posséder le matériel nécessaire pour effectuer les livraisons.'+
            ' Une partie des frais est à la charge du livreur deliv’s(vélo ou bien scooter avec l’équipement de sécurité exigé par le code de la route,'+
            ' son entretien, un smartphone suffisamment récent et un forfait mobile).</li>'+
            '<li>Phase de recrutement: réunion de prise de contact, vérification du matériel, période d’essai et éventuellement signature du contrat.</li>'+
            '</ul><p>Pour mieux connaitre les prochaines étapes,appelez-nous au +216 98 810 840.</p></div><div style="max-width: 1000;margin:auto;text-align:center" >'+
            '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
            /* attachments: [ 

                {                  
                 path: 'https://marketplaceupload.s3.eu-west-3.amazonaws.com/Contrat-livreur.pdf', 
                 contentType: "application/pdf" 
         
                }  
               ] */ 
                               
        };
        smtpTransport.sendMail(mailOptions, (error, response) => {
            res.send(true);
            smtpTransport.close();
        })
    })
  }
  // Create and Save a new Note
exports.confirmMail = (req, res) => {


    // Find a matching token
    Token.findOne({ token: req.body.token }).then(token=>{
        if(token){
            DeliveryMan.findOne({ _id: token._userId }).then(deliveryman=>{
                var verified = deliveryman.verified;
                deliveryman.verified=true;
                deliveryman.save().then(data=>{
                    if(verified){res.send(data);}
                    else {
                    const mailOptions = {
                        from: "contact@delivs.tn",
                        to: deliveryman.email,
                        subject: "Bienvenue",
                        generateTextFromHTML: true,
                        html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
                        '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
                        '<h3 style="margin-top:5%;color:#6861e1">Bonjour '+deliveryman.firstName+'</h3>'+
                        '</div><div style="max-width: 1000;margin:auto;" >'+
                        '<p>Plutôt que de travailler derrière un bureau, exercer un job en plein air vous tente?'+
                        ' Vous aimez rouler, vous êtes flexibles et aimez le contact humain? Pourquoi ne pas devenir livreur deliv’s?</p>'+
                        '<p>Si vous êtes conquis à l’idée de travailler chez deliv’s vous vous demandez sans doute comment devenir livreur'+
                        ' deliv’s. Nous vous expliquons la démarche à suivre pour y parvenir.</p>'+
                        '<p>Ce qu’il vous faut:</p><ul><li>Être majeur et disposer d\'un statut d\'entrepreneur individuel :'+
                        'Vous devez avoir plus de 18 ans et avoir le droit de travailler en Tunisie.</li>'+
                        '<li>Un smartphone : iPhone iOS 10+ ou Android 5+</li>'+
                        '<li>Un vélo ou un scooter:Choisissez votre mode de transport préféré pour livrer en ville</li></ul>'+
                        '<p>Pour postuler à une offre d’emploi et devenir livreur deliv’s il y a plusieurs modalités à effectuer :</p>'+
                        '<ul><li>Inscription sur la plateforme en ligne deliv’s: remplir un formulaire d’inscription deliv’s</li>'+
                        '<li>Se procurer un équipement complet: devenir livreur deliv’s implique de posséder le matériel nécessaire pour effectuer les livraisons.'+
                        ' Une partie des frais est à la charge du livreur deliv’s(vélo ou bien scooter avec l’équipement de sécurité exigé par le code de la route,'+
                        ' son entretien, un smartphone suffisamment récent et un forfait mobile).</li>'+
                        '<li>Phase de recrutement: réunion de prise de contact, vérification du matériel, période d’essai et éventuellement signature du contrat.</li>'+
                        '</ul><p>Pour mieux connaitre les prochaines étapes,appelez-nous au +216 98 810 840.</p></div><div style="max-width: 1000;margin:auto;text-align:center" >'+
                        '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
                        /* attachments: [ 
 
                            {                  
                             path: 'https://marketplaceupload.s3.eu-west-3.amazonaws.com/Contrat-livreur.pdf', 
                             contentType: "application/pdf" 
                     
                            }  
                           ] */ 
                                           
                    };
                    smtpTransport.sendMail(mailOptions, (error, response) => {
                        res.send(data);
                        smtpTransport.close();
                    })}}).catch(err=>{
                    res.status(500).send({
                        message: err.message || "Some error occurred while updating the deliveryman."
                    });
                })
            }).catch(err=>{return res.status(400).send({ msg: 'We were unable to find a user for this token.' });})
        }
        else {
            DeliveryMan.findOne({email:req.body.email})
            .then(deliveryMan => {
                var token = new Token({ _userId: deliveryMan._id, token: crypto.randomBytes(16).toString('hex') });
                token.save().then(tokenObject=>{
                    const mailOptions = {
                    from: "contact@delivs.tn",
                    to: deliveryMan.email,
                    subject: "Confirmation ",
                    generateTextFromHTML: true,
                    html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
                    '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
                    '<h3 style="margin-top:5%;color:#6861e1">Bonjour '+deliveryMan.firstName+'</h3>'+
                    '<p>Le lien d\'activation a été expiré</p>'+
                    '<p>Pour activer votre compte, veuillez cliquer sur le lien ci dessous </p>'+
                    '<p><a href="' + req.body.url + '/'+tokenObject.token+'/'+deliveryMan.email+'"> Lien d\'activation </a></p>'+
                    '<p>L\'activation du compte vous permmet de modifier vos infromations en attendant la validation finale par l\'administrateur</p>'+
                    '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
                
                    
                };
        
            smtpTransport.sendMail(mailOptions, (error, response) => {
                if(error){return res.status(500).send(error)}
                if(response){return res.status(200).send(false)}
                smtpTransport.close();
            })
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Token."
            });
        })
                
                
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving Authentification."
                });
            })
            
        }
    }).catch(err=>{
        return res.status(400).send({ type: 'not-verified', msg: 'We were unable to find a valid token. Your token my have expired.' });
    })
};
// Create and Save a new Deliveryman
exports.create = (req, res) => {
        // Validate request
        const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

        if (!errors.isEmpty()) {
          res.status(422).json({ errors: errors.array() });
          return;
        }
    if(!req.body) {
        return res.status(400).send({
            message: "DeliveryMan can not be empty"
        });
    }

             // Create a deliveryMan
    const deliveryMan = new DeliveryMan({
     
        firstName: req.body.firstName || null,
        lastName: req.body.lastName || null,
        phoneNumber: req.body.phoneNumber || null,
        address: req.body.address || null,
        zone: req.body.zone || "Autres",
        email: req.body.email || null,
        password:req.body.password || null,
        active: req.body.active ,
        available: req.body.available ,
        verified: true,
        //verified: req.body.verified
        connectionInfo:{connected:false,connectionDate:new Date(),connectionSource:"none"},
        coordinates: req.body.coordinates || {latitude:"",longitude:""},
        token: Math.floor(Math.random() * (99999-10000)+10000)

    });
    // check email is unique
    DeliveryMan.findOne({email:deliveryMan.email})
    .then(deliveryM => {
        if(deliveryM){
            //Email exists
            res.send(true);
        }
        else {
    // Save deliveryMan in the database
    deliveryMan.save()
    .then(data => {
                // Create a verification token for this user
        var token = new Token({ _userId: data._id, token: crypto.randomBytes(16).toString('hex') });
        token.save().then(tokenObject=>{
/*             const mailOptions = {
                from: "contact@delivs.tn",
                to: req.body.email,
                subject: "Activer votre compte livreur Deliv's",
                generateTextFromHTML: true,
                html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
                '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
                '<h3 style="margin-top:5%;color:#6861e1">Activer votre compte Deliv\'s</h3>'+
                '<p>Pour activer votre compte, veuillez cliquer sur le lien ci dessous </p>'+
                '<p><a href="' + req.body.url + '/'+tokenObject.token+'/'+data.email+'"> Lien d\'activation </a></p>'+
                '<p>L\'activation du compte vous permmet de modifier vos infromations en attendant la validation finale par l\'administrateur</p>'+
                '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
            
                
            };
            smtpTransport.sendMail(mailOptions, (error, response) => {
                smtpTransport.close();
            }) */
            res.send(data) ; 
        }).catch(err => {
            res.status(500).send({
                message:  "Some error occurred while creating the Token."
            });
        })
        
    }).catch(err => {
        res.status(500).send({
            message: "Some error occurred while creating the DeliveryMan."
        });
    });
        }
    
    })
       
   



};
// Create and Save a new Deliveryman
exports.createSocial = (req, res) => {

if(!req.body) {
    return res.status(400).send({
        message: "DeliveryMan can not be empty"
    });
}

// Create a deliveryMan
const deliveryMan = new DeliveryMan({
 
    firstName: req.body.firstName || null,
    lastName: req.body.lastName || null,
    phoneNumber: req.body.phoneNumber || null,
    zone: req.body.zone || "Autres",
    address:  "",
    email: req.body.email || null,
    password:req.body.password || null,
    active:  false,
    available:  true,
    verified:  true,
    connectionInfo:{connected:false,connectionDate:new Date(),connectionSource:"none"},
    coordinates: {latitude:"",longitude:""}
});
// check email is unique
DeliveryMan.findOne({email:deliveryMan.email})
.then(deliveryM => {
    if(deliveryM){
        //Email exists
        res.send(true);
    }
    else {
// Save deliveryMan in the database
deliveryMan.save()
.then(data => {res.send(data);

}).catch(err => {
    res.status(500).send({
        message: err.message || "Some error occurred while creating the DeliveryMan."
    });
});
    }

})



};
// Check if the deliveryMan existe by its mail and password 
exports.login =(req,res) => {
    DeliveryMan.findOne({email:req.params.email,password:req.params.pass})
    .then(deliveryMan => {
        res.send(deliveryMan);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving Authentification."
        });
    });
};
// Check if the DeliveryMan existe by its mail  
exports.findOneByMail =(req,res) => {
    DeliveryMan.findOne({email:req.params.mail})
    .then(deliveryMan => {
        res.send(deliveryMan);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving the sender."
        });
    });
};


// Retrieve and return all deliveryMan from the database.
exports.findAll = (req, res) => {
    DeliveryMan.find()
    .then(deliveryMan => {
        res.send(deliveryMan);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving DeliveryMan."
        });
    });
};

// Find a single deliveryMan with a deliveryManId
exports.findOne = (req, res) => {
    DeliveryMan.findById(req.params.deliveryManId)
    .then(deliveryMan => {
        if(!deliveryMan) {
            return res.status(404).send({
                message: "DeliveryMan not found with id " + req.params.deliveryManId
            });            
        }
        res.send(deliveryMan);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "DeliveryMan not found with id " + req.params.deliveryManId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving DeliveryMan with id " + req.params.deliveryManId
        });
    });
};

// Update a deliveryMan identified by the deliveryMan in the request
exports.update = (req, res) => {
        // Validate request
        const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

        if (!errors.isEmpty()) {
          res.status(422).json({ errors: errors.array() });
          return;
        }
    if(!req.body) {
        return res.status(400).send({
            message: "can not be empty"
        });
    }

    // Find deliveryMan and update it with the request body
    DeliveryMan.findByIdAndUpdate(req.params.deliveryManId, {
        firstName: req.body.firstName || null,
        lastName: req.body.lastName || null,
        phoneNumber: req.body.phoneNumber || null,
        address: req.body.address || null,
        zone: req.body.zone || "Autres",
        password:req.body.password || null,
        active: req.body.active ,
        available: req.body.available ,
        connectionInfo:req.body.connectionInfo || {connected:false,connectionDate:new Date(),connectionSource:"none"},
        token: req.body.token 


    }, {new: true})
    .then(deliveryMan => {
        if(!deliveryMan) {
            return res.status(404).send({
                message: "DeliveryMan not found with id " + req.params.deliveryManId
            });
        }
        res.send(deliveryMan);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "DeliveryMan not found with id " + req.params.deliveryManId
            });                
        }
        return res.status(500).send({
            message: "Error updating DeliveryMan with id " + req.params.deliveryManId
        });
    });
};
// Update a deliveryMan'location identified by the deliveryMan id the request
exports.updateLocation = (req, res) => {

if(!req.body) {
    return res.status(400).send({
        message: "can not be empty"
    });
}

// Find deliveryMan and update it with the request body
DeliveryMan.findByIdAndUpdate(req.params.deliveryManId, {
    coordinates: req.body.coordinates || null,
}, {new: true})
.then(deliveryMan => {
    console.log(deliveryMan)
    if(!deliveryMan) {
        return res.status(404).send({
            message: "DeliveryMan not found with id " + req.params.deliveryManId
        });
    }
    res.send(deliveryMan);
}).catch(err => {
    if(err.kind === 'ObjectId') {
        return res.status(404).send({
            message: "DeliveryMan not found with id " + req.params.deliveryManId
        });                
    }
    return res.status(500).send({
        message: "Error updating DeliveryMan with id " + req.params.deliveryManId
    });
});
};

// Delete a deliveryMan with the specified deliveryManId in the request
exports.delete = (req, res) => {
    DeliveryMan.findByIdAndRemove(req.params.deliveryManId)
    .then(deliveryMan => {
        if(!deliveryMan) {
            return res.status(404).send({
                message: "DeliveryMan not found with id " + req.params.deliveryManId
            });
        }
        res.send({message: "DeliveryMan deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "DeliveryMan not found with id " + req.params.deliveryManId
            });                
        }
        return res.status(500).send({
            message: "Could not delete DeliveryMan with id " + req.params.deliveryManId
        });
    });
};
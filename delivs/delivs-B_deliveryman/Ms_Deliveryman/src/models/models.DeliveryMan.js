const mongoose = require('mongoose');
var mongooseToCsv = require('mongoose-to-csv');

const DeliveryManSchema = mongoose.Schema({
	token:String,
    firstName:      String,
    lastName:      String,
	phoneNumber:   String,
	address: String,
	zone:String,
	coordinates:{
		latitude:String,
		longitude:String
	},
	email: String,
	password: String,
	active: Boolean,
	available: Boolean,
	verified: Boolean,
	connectionInfo: {
		connected:Boolean,
		connectionDate:Date,
		connectionSource:String
	},
}, {
    timestamps: true
});
DeliveryManSchema.plugin(mongooseToCsv, {
	delimiter : ';',
	headers: '_id firstName lastName phoneNumber address zone coordinates_latitude coordinates_longitude connectionInfo_connected connectionInfo_connectionDate connectionInfo_connectionSource email available verified active createdAt updatedAt',
	virtuals: {
		'coordinates_latitude': function(doc) {
			return doc.coordinates.latitude;
		  },
		  'coordinates_longitude': function(doc) {
			  return doc.coordinates.longitude;
		  },
		'connectionInfo_connected': function(doc) {
			if(!doc.connectionInfo){
				return 0;
			}
			return doc.connectionInfo.connected;
		},
		'connectionInfo_connectionDate': function(doc) {
			if(!doc.connectionInfo){
				return 0;
			}
			let d = new Date(doc.connectionInfo.connectionDate);
			return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+" "+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();;
		},
		'connectionInfo_connectionSource': function(doc) {
			if(!doc.connectionInfo){
				return 0;
			}
			return doc.connectionInfo.connectionSource;
		},
		'createdAt': function(doc) {
		  return new Date(doc.createdAt).getDate()+'/'+(new Date(doc.createdAt).getMonth()+1)+'/'+new Date(doc.createdAt).getFullYear();
	  },
	  'updatedAt': function(doc) {
		  return new Date(doc.updatedAt).getDate()+'/'+(new Date(doc.updatedAt).getMonth()+1)+'/'+new Date(doc.updatedAt).getFullYear();
	}
	  }
	
  });
module.exports = mongoose.model('DeliveryMan', DeliveryManSchema);
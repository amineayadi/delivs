module.exports = (app) => {
    const DeliveryMan = require('../controllers/controllers.DeliveryMan');
    const upload = require('../models/upload')
  // export data CSV
  app.get('/exportData', DeliveryMan.exportData);
    // Upload an image
    app.post('/upload',upload.single('image'),DeliveryMan.upload);
     // welcomeMail
     app.post('/welcomeMail', DeliveryMan.welcomeMail);
     // sendemail
     app.post('/confirmMail', DeliveryMan.confirmMail);
      // sendemail
      app.get('/confirmMail/:id', DeliveryMan.confirmMailSocial);
    // Create a new DeliveryMan
    app.post('/DeliveryMan',DeliveryMan.validate('create'), DeliveryMan.create);
    // Create a new DeliveryMan
    app.post('/DeliveryManSocial', DeliveryMan.createSocial);
    // Login
    app.get('/login/:email/:pass', DeliveryMan.login);

    // Retrieve all DeliveryMan
    app.get('/DeliveryMan', DeliveryMan.findAll);

    // Retrieve a single DeliveryMan with deliveryManId
    app.get('/DeliveryMan/:deliveryManId', DeliveryMan.findOne);

    // Retrieve a single DeliveryMan with mail
    app.get('/getByMail/:mail', DeliveryMan.findOneByMail);

    // Update a DeliveryMan with deliveryManId
    app.put('/DeliveryMan/:deliveryManId',DeliveryMan.validate('update'), DeliveryMan.update);

      // Update a DeliveryMan;location with deliveryManId
    app.put('/DeliveryManLocation/:deliveryManId', DeliveryMan.updateLocation);

    // Delete a DeliveryMan with deliveryManId
    app.delete('/DeliveryMan/:deliveryManId', DeliveryMan.delete);
}
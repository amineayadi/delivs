const Eureka = require('eureka-js-client').Eureka;

const client = new Eureka({
  instance: {
    instanceId: 'NOTESERVICE',
    app: 'NOTESERVICE',
    hostName: 'delivery-ms-note',
    ipAddr: 'delivery-ms-note',
    statusPageUrl: 'http://delivery-ms-note:3005',
    port: {
      '$': 3005,
      '@enabled': 'true',
    },
    vipAddress: 'NOTESERVICE',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    registerWithEureka: true,
    fetchRegistry: true,
  },
  eureka: {
    host: 'delivery-eureka-service',
    port: 8763,
    servicePath: '/eureka/apps/',
  }
});
client.logger.level('debug');
module.exports = client
const Note = require('../models/note');
const { body,check, validationResult } = require('express-validator');
var s3_r = require('../config/aws-reporting')
const BUCKET_R = 'delivs-reporting'
var fs = require('fs');
exports.validate = (method) => {
    switch (method) {
      case 'create': {
       return [ 
        check('delivery').exists().withMessage('delivery required').isString().withMessage('Invalid delivery'),
        check('recipient').exists().withMessage('recipient required').isString().withMessage('Invalid recipient'),
        check('note','Invalid note').exists().isIn([1,2,3,4,5]),
        check('comment','Invalid comment').exists().isString() ,
        check('dateNote','invalid dateNote').exists()    
    ]   
      }
      case 'update': {
        return [ 
            check('delivery').exists().withMessage('delivery required').isString().withMessage('Invalid delivery'),
            check('recipient').exists().withMessage('recipient required').isString().withMessage('Invalid recipient'),
            check('note','Invalid note').exists().isIn([1,2,3,4,5]),
            check('comment','Invalid comment').exists().isString() ,
            check('dateNote','invalid dateNote').exists()   
     ]   
       }
    }
  }
  exports.exportData = (req, res) => {
    const filename = 'delivs-notes-input.csv'
    const storage = 'uploads/'
    Note.find().stream().pipe(Note.csvTransformStream())
    .pipe(fs.createWriteStream(storage+filename)).on('finish', function() {
        s3_r.putObject({
            Bucket: BUCKET_R,
            Body: fs.readFileSync(storage+filename),
            Key: filename,
            ACL:'public-read',
            Expires:new Date(2040,12,31,0,0,0,0)
          })
            .promise()
            .then(response => { res.send(`${s3_r.getSignedUrl('getObject', { Bucket: BUCKET_R, Key: filename })}`.split("?").shift().toString())
            })
            .catch(err => {
                res.send(false)
              console.log('failed:', err)
            })
    })

   

};
// Create and Save a new Note
exports.create = (req, res) => {
            // Validate request
            const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

            if (!errors.isEmpty()) {
              res.status(422).json({ errors: errors.array() });
              return;
            }
    if(!req.body.note) {
        return res.status(400).send({
            message: "Note  can not be empty"
        });
    }

    // Create a Note
    const note = new Note({
        delivery: req.body.delivery || null , 
        recipient: req.body.recipient || null ,
        accountId: req.body.accountId|| null , 
        accountType: req.body.accountType|| null ,
        note: req.body.note|| null , 
        comment: req.body.comment|| null , 
        dateNote: req.body.dateNote|| null 
    });

    // Save Note in the database
    note.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Note."
        });
    });
};

// Retrieve and return all notes from the database.
exports.findAll = (req, res) => {
    Note.find()
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Retrieve and return all notes belonnig to a recipient.
exports.findByRecipient = (req, res) => {
    Note.find({recipient:req.params.idRecipient})
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Retrieve and return all notes belonnig to a delivery.
exports.findByDelivery = (req, res) => {
    Note.find({delivery:req.params.idDelivery})
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Retrieve and return all notes belonnig to a deliveryman.
exports.findByDeliveryMan = (req, res) => {
    Note.find({accountId:req.params.idDeliveryMan,accountType:'deliveryman'})
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Retrieve and return average of notes belonnig to a deliveryman.
exports.AverageByDeliveryMan = (req, res) => {
    Note.find({accountId:req.params.idDeliveryMan,accountType:'deliveryman'})
    .then(notes => {
        var sum =0;
        var ave=5;
        if(notes.length!=0){
        notes.forEach(item=>{sum+=item.note});       
        ave = sum/notes.length;}
         res.send(ave.toString());
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Retrieve and return all notes belonnig to a sender.
exports.findBySender= (req, res) => {
    Note.find({accountId:req.params.idSender,accountType:'sender'})
    .then(notes => {
        res.send(notes);
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Retrieve and return average of notes belonnig to a sender.
exports.AverageBySender = (req, res) => {
    Note.find({accountId:req.params.idSender,accountType:'sender'})
    .then(notes => {
        var sum =0;
        var ave=5;
        if(notes.length!=0){
        notes.forEach(item=>{sum+=item.note});       
        ave = sum/notes.length;}
         res.send(ave.toString());
    }).catch(err => {
        res.status(500).send({  
            message: err.message || "Some error occurred while retrieving notes."
        });
    });
};
// Find a single note with a noteId
exports.findOne = (req, res) => {
    Note.findById(req.params.noteId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });            
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving note with id " + req.params.noteId
        });
    });
};

// Update a note identified by the noteId in the request
exports.update = (req, res) => {
            // Validate request
            const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

            if (!errors.isEmpty()) {
              res.status(422).json({ errors: errors.array() });
              return;
            }
    if(!req.body.note) {
        return res.status(400).send({
            message: "Note content can not be empty"
        });
    }

    // Find note and update it with the request body
    Note.findByIdAndUpdate(req.params.noteId, {
        
        delivery: req.body.delivery || null , 
        accountId: req.body.accountId|| null , 
        recipient: req.body.recipient || null ,
        accountType: req.body.accountType|| null ,
        recipient: req.body.recipient|| null , 
        note: req.body.note|| null , 
        comment: req.body.comment|| null , 
        dateNote: req.body.dateNote|| null 
    }, {new: true})
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        res.send(note);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Error updating note with id " + req.params.noteId
        });
    });
};

// Delete a note with the specified noteId in the request
exports.delete = (req, res) => {
    Note.findByIdAndRemove(req.params.noteId)
    .then(note => {
        if(!note) {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });
        }
        res.send({message: "Note deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Note not found with id " + req.params.noteId
            });                
        }
        return res.status(500).send({
            message: "Could not delete note with id " + req.params.noteId
        });
    });
};
const mongoose = require('mongoose');
var mongooseToCsv = require('mongoose-to-csv');

const NoteSchema = mongoose.Schema({
    
    delivery: String,
    recipient: String,
    accountId:String,
    accountType:String,
    note: Number  ,
    comment : String,
    dateNote: Date

}, );
NoteSchema.plugin(mongooseToCsv, {
	delimiter : ';',
  headers: '_id delivery recipient accountId accountType note comment dateNote createdAt updatedAt',
  virtuals: {
		'dateNote': function(doc) {
			if(!doc.connectionInfo){
				return 0;
			}
			let d = new Date(doc.dateNote);
			return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+" "+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();;
		},
			
		'createdAt': function(doc) {
		  return new Date(doc.createdAt).getDate()+'/'+(new Date(doc.createdAt).getMonth()+1)+'/'+new Date(doc.createdAt).getFullYear();
	  },
	  'updatedAt': function(doc) {
		  return new Date(doc.updatedAt).getDate()+'/'+(new Date(doc.updatedAt).getMonth()+1)+'/'+new Date(doc.updatedAt).getFullYear();
	}
	  }

	
  });
module.exports = mongoose.model('Note', NoteSchema);
module.exports = (app) => {
    const notes = require('../controllers/note.controller');

    // export data CSV
    app.get('/exportData', notes.exportData);
    // Create a new Note
    app.post('/note',notes.validate('create'), notes.create);

    // Retrieve all Notes
    app.get('/note', notes.findAll);

     // Retrieve all Notes belonnig to a delivery.
     app.get('/noteByDelivery/:idDelivery', notes.findByDelivery);

   // Retrieve all Notes belonnig to a recipient.
    app.get('/noteByRecipient/:idRecipient', notes.findByRecipient);
   
    // Retrieve all Notes belonnig to a deliveryman.
   app.get('/noteByDeliveryMan/:idDeliveryMan', notes.findByDeliveryMan);

     // Retrieve average of notes belonnig to a deliveryman.
     app.get('/averageByDeliveryMan/:idDeliveryMan', notes.AverageByDeliveryMan);

      // Retrieve all Notes belonnig to a sender.
      app.get('/noteBySender/:idSender', notes.findBySender);

      // Retrieve average of notes belonnig to a sender.
      app.get('/AverageBySender/:idSender', notes.AverageBySender);

    // Retrieve a single Note with noteId
    app.get('/note/:noteId', notes.findOne);

    // Update a Note with noteId
    app.put('/note/:noteId',notes.validate('update'),  notes.update);

    // Delete a Note with noteId
    app.delete('/note/:noteId', notes.delete);
}
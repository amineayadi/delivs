var ip = require('./env.js');
const Eureka = require('eureka-js-client').Eureka;

const client = new Eureka({
  instance: {
    instanceId: 'ORDERSERVICE',
    app: 'ORDERSERVICE',
    hostName: 'delivery-ms-order',
    ipAddr: 'delivery-ms-order',
    statusPageUrl: 'http://delivery-ms-order:3004',
    port: {
      '$': 3004,
      '@enabled': 'true',
    },
    vipAddress: 'ORDERSERVICE',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    registerWithEureka: true,
    fetchRegistry: true,
  },
  eureka: {
    host: 'delivery-eureka-service',
    port: 8763,
    servicePath: '/eureka/apps/',
  }
});
client.logger.level('debug');
module.exports = client
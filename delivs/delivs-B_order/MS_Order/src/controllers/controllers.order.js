const Order = require('../models/models.order');
const { body,check, validationResult } = require('express-validator');
exports.validate = (method) => {
    switch (method) {
      case 'create': {
       return [ 
        check('total').exists().withMessage('total required').isNumeric().withMessage('Invalid total'),
        check('customer').exists().withMessage('customer required').isString().withMessage('Invalid customer'),
        check('resto').exists().withMessage('resto required').isString().withMessage('Invalid resto'),
        body('orderItems','invalid orderItems ').exists().isArray().isLength({min:1}),
        body('orderItems.*.product','Invalid product').exists().isString(),
        body('orderItems.*.quantity','Invalid quantity').exists().isInt() ,
        body('orderItems.*.price','Invalid price').exists().isNumeric()     
    ]   
      }
      case 'update': {
        return [ 
            check('total').exists().withMessage('total required').isNumeric().withMessage('Invalid total'),
            check('customer').exists().withMessage('customer required').isString().withMessage('Invalid customer'),
            check('resto').exists().withMessage('resto required').isString().withMessage('Invalid resto'),
            body('orderItems','invalid orderItems ').exists().isArray().isLength({min:1}),
            body('orderItems.*.product','Invalid product').exists().isString(),
            body('orderItems.*.quantity','Invalid quantity').exists().isInt() ,
            body('orderItems.*.price','Invalid price').exists().isNumeric()      
     ]   
       }
    }
  }
// Create and Save a new order
exports.create = (req, res) => {
            // Validate request
            const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

            if (!errors.isEmpty()) {
              res.status(422).json({ errors: errors.array() });
              return;
            }
    if(!req.body) {
        return res.status(400).send({
            message: "Order can not be empty"
        });
    }
    // Create a order
    const order = new Order({
        total:req.body.total,
        totalDelivs:req.body.totalDelivs,
        customer: req.body.customer,
        resto: req.body.resto,
        orderItems:req.body.orderItems

    });

    // Save order in the database
    order.save()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the order."
        });
    });
};


// Retrieve and return all orders from the database.
exports.findAll = (req, res) => {
    Order.find()
    .then(data => {
        res.send(data);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving orders."
        });
    });
};

// Find a single order with an id
exports.findOne = (req, res) => {
    Order.findById(req.params.orderId)
    .then(order => {
        if(!order) {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });            
        }
        res.send(order);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Order with id " + req.params.orderId
        });
    });
};

// Update an Order identified by the id in the request
exports.update = (req, res) => {
            // Validate request
            const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

            if (!errors.isEmpty()) {
              res.status(422).json({ errors: errors.array() });
              return;
            }
    if(!req.body) {
        return res.status(400).send({
            message: "can not be empty"
        });
    }

    // Find Order and update it with the request body
    Order.findByIdAndUpdate(req.params.orderId, {

        total:req.body.total,
        totalDelivs:req.body.totalDelivs,
        customer: req.body.customer,
        resto: req.body.resto,
        orderItems:req.body.orderItems

    }, {new: true})
    .then(order => {
        if(!order) {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });
        }
        res.send(order);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });                
        }
        return res.status(500).send({
            message: "Error updating Order with id " + req.params.orderId
        });
    });
};


// Delete an Order with the specified Id in the request
exports.delete = (req, res) => {
    Order.findByIdAndRemove(req.params.orderId)
    .then(order => {
        if(!order) {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });
        }
        res.send({message: "Order deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Order not found with id " + req.params.orderId
            });                
        }
        return res.status(500).send({
            message: "Could not delete Order with id " + req.params.orderId
        });
    });
};
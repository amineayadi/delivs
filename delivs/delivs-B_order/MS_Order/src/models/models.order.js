const mongoose = require('mongoose');

const OrderSchema = mongoose.Schema({

	total:Number,
	totalDelivs:Number,
	customer:String,
	resto:String,
	orderItems:[
		{
			product:String,
			quantity:Number,
			price:Number,
			delivs:Number,
			comment:String
		}
	]

	
}, {
    timestamps: true
});

module.exports = mongoose.model('Order', OrderSchema);


module.exports = (app) => {
    const order = require('../controllers/controllers.order');

    // Create a new order 
    app.post('/order', order.validate('create'),order.create);

    // Retrieve all orders
    app.get('/order', order.findAll);

    // Retrieve a single order with id
    app.get('/order/:orderId', order.findOne);

    // Update a order with id
    app.put('/order/:orderId', order.validate('update'), order.update);

    // Delete a order with id
    app.delete('/order/:orderId', order.delete);
}
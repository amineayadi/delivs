const Eureka = require('eureka-js-client').Eureka;

const client = new Eureka({
  instance: {
    instanceId: 'RECIPIENTSERVICE',
    app: 'RECIPIENTSERVICE',
    hostName: 'delivery-ms-recipient',
    ipAddr: 'delivery-ms-recipient',
    statusPageUrl: 'http://delivery-ms-recipient:3003',
    port: {
      '$': 3003,
      '@enabled': 'true',
    },
    vipAddress: 'RECIPIENTSERVICE',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    registerWithEureka: true,
    fetchRegistry: true,
  },
  eureka: {
    host: 'delivery-eureka-service',
    port: 8763,
    servicePath: '/eureka/apps/',
  }
});
client.logger.level('debug');
module.exports = client
const Recipient = require('../models/models.recipient');
const { body,check, validationResult } = require('express-validator');
const { smtpTransport}= require('../config/mail');
var crypto = require('crypto')
const Token = require('../models/token');
var fs = require('fs');
var s3_r = require('../config/aws-reporting')
const BUCKET_R = 'delivs-reporting'
exports.validate = (method) => {
    switch (method) {
      case 'create': {
        return [ 
            check('firstName').exists(),
            check('lastName').exists(),
            check('email').exists().isEmail().withMessage('Invalid email'),
            check('phoneNumber').exists().isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid phoneNumber'),
            check('address').exists().isLength({max:100 }).withMessage('Address Too Long'),
          //  check('cin').exists().isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid CIN'),
          //  check('rib').optional({nullable: true}).isNumeric().isLength({ min: 20,max:20 }).withMessage('Invalid RIB'),
            check('password').exists().isLength({ min: 5 }).withMessage('must be at least 5 chars long').matches(/\d/).withMessage('must contain a number')  
        ]   
      }
      case 'update': {
        return [ 
         check('firstName').exists(),
         check('lastName').exists(),
         check('email').exists().isEmail().withMessage('Invalid email'),
         check('phoneNumber').optional({nullable: true}).exists().isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid phoneNumber'),
         check('address').optional({nullable: true}).exists().isLength({max:100 }).withMessage('Address Too Long'),
       //  check('cin').exists().isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid CIN'),
       //  check('rib').optional({nullable: true}).isNumeric().isLength({ min: 20,max:20 }).withMessage('Invalid RIB'),
         check('password').exists().isLength({ min: 5 }).withMessage('must be at least 5 chars long').matches(/\d/).withMessage('must contain a number')  
     ]   
       }
    }
  }
  exports.exportData = (req, res) => {
    const filename = 'delivs-recipient-input.csv'
    const storage = 'uploads/'
    Recipient.find().stream().pipe(Recipient.csvTransformStream())
    .pipe(fs.createWriteStream(storage+filename)).on('finish', function() {
        s3_r.putObject({
            Bucket: BUCKET_R,
            Body: fs.readFileSync(storage+filename),
            Key: filename,
            ACL:'public-read',
            Expires:new Date(2040,12,31,0,0,0,0)
          })
            .promise()
            .then(response => { res.send(`${s3_r.getSignedUrl('getObject', { Bucket: BUCKET_R, Key: filename })}`.split("?").shift().toString())
            })
            .catch(err => {
                res.send(false)
              console.log('failed:', err)
            })
    })

   

};
  // confirm mail
  exports.confirmMail = (req, res) => {


    // Find a matching token
    Token.findOne({ token: req.body.token }).then(token=>{
        if(token){
            Recipient.findOne({ _id: token._userId }).then(recipient=>{
                var verified = recipient.verified;
                recipient.verified=true;
                recipient.save().then(data=>{
                    if(verified){res.send(data);}
                    const mailOptions = {
                        from: "contact@delivs.tn",
                        to: recipient.email,
                        subject: "Bienvenue",
                        generateTextFromHTML: true,
                        html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
                        '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
                        '<h3 style="margin-top:5%;color:#6861e1">Bonjour '+recipient.firstName+'</h3>'+
                        '<p>votre compte a été activé avec succès</p>'+
                        '<p>vous pouvez maintenant se connecter et commander votre plat preferè </p>'+
                        '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
                                           
                    };
                    smtpTransport.sendMail(mailOptions, (error, response) => {
                        res.send(data);
                        smtpTransport.close();
                    }) 
                }).catch(err=>{
                    res.status(500).send({
                        message: err.message || "Some error occurred while updating the deliveryman."
                    });
                })
            }).catch(err=>{return res.status(400).send({ msg: 'We were unable to find a user for this token.' });})
        }
        else {
            Recipient.findOne({email:req.body.email})
            .then(recipient => {
                var token = new Token({ _userId: recipient._id, token: crypto.randomBytes(16).toString('hex') });
                token.save().then(tokenObject=>{
                    const mailOptions = {
                    from: "contact@delivs.tn",
                    to: recipient.email,
                    subject: "Confirmation ",
                    generateTextFromHTML: true,
                    html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
                    '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
                    '<h3 style="margin-top:5%;color:#6861e1">Bonjour '+recipient.name+'</h3>'+
                    '<p>Le lien d\'activation a été expiré</p>'+
                    '<p>Pour activer votre compte, veuillez cliquer sur le lien ci dessous </p>'+
                    '<p><a href="' + req.body.url + '/'+tokenObject.token+'/'+recipient.email+'"> Lien d\'activation </a></p>'+
                    '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
                
                    
                };
      
            smtpTransport.sendMail(mailOptions, (error, response) => {
                res.send(false);
                smtpTransport.close();
            })
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Token."
            });
        })
                
                
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving Authentification."
                });
            })        }
    }).catch(err=>{
        return res.status(400).send({ type: 'not-verified', msg: 'We were unable to find a valid token. Your token my have expired.' });
    })
};
// Create and Save a new Recipient
exports.create = (req, res) => {
          // Validate request
          const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

          if (!errors.isEmpty()) {
            res.status(422).json({ errors: errors.array() });
            return;
          }
    if(!req.body) {
        return res.status(400).send({
            message: "Recipient can not be empty"
        });
    }

              // Create a Recipient
    const recipient = new Recipient({
     
        firstName: req.body.firstName || null,
        lastName: req.body.lastName || null,
        phoneNumber: req.body.phoneNumber || null,
        address: req.body.address || null,
        zone: req.body.zone || "Autres",
        email: req.body.email || null,
        password:req.body.password || null,
        type: req.body.type || null,
        //verified: false,
        verified: true,
        connectionInfo:{connected:false,connectionDate:new Date(),connectionSource:"none"},
        token: Math.floor(Math.random() * (99999-10000)+10000),
    });
    // check email is unique
    Recipient.findOne({email:recipient.email})
    .then(recp => {
        if(recp){
            //Email exists
            res.send(true);
        }
        else {
    // Save Recipient in the database
    recipient.save()
    .then(data => {
        res.send(data) ;  
        // Create a verification token for this user
/* var token = new Token({ _userId: data._id, token: crypto.randomBytes(16).toString('hex') });
token.save().then(tokenObject=>{
    const mailOptions = {
        from: "contact@delivs.tn",
        to: req.body.email,
        subject: "Activer votre compte  Deliv's",
        generateTextFromHTML: true,
        html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
        '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
        '<h3 style="margin-top:5%;color:#6861e1">Activer votre compte Deliv\'s</h3>'+
        '<p>Pour activer votre compte, veuillez cliquer sur le lien ci dessous </p>'+
        '<p><a href="' + req.body.url + '/'+tokenObject.token+'/'+data.email+'"> Lien d\'activation </a></p>'+
        '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
    
    };
    smtpTransport.sendMail(mailOptions, (error, response) => {                if(response){res.status(200).send(data)}
        smtpTransport.close();
    })
    
}).catch(err => {
    res.status(500).send({
        message: err.message || "Some error occurred while creating the Token."
    });
}) */

}).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while creating the Recipient."
        });
    });
        }
    
    })
       
  

};
// Create and Save a new Recipient
exports.createSocial = (req, res) => {
if(!req.body.email || !req.body.password) {
  return res.status(400).send({
      message: "Recipient can not be empty"
  });
}
// Create a Recipient
const recipient = new Recipient({

  firstName: req.body.firstName || null,
  lastName: req.body.lastName || null,
  phoneNumber: req.body.phoneNumber || null,
  address:  "",
  zone: req.body.zone || "Autres",
  email: req.body.email || null,
  password:req.body.password || null,
  type: req.body.type || null,
  verified: true,
  connectionInfo:{connected:false,connectionDate:new Date(),connectionSource:"none"}
});
// check email is unique
Recipient.findOne({email:recipient.email})
.then(recp => {
  if(recp){
      //Email exists
      res.send(true);
  }
  else {
// Save Recipient in the database
recipient.save()
.then(data => {
  res.send(data);
}).catch(err => {
  res.status(500).send({
      message: err.message || "Some error occurred while creating the Recipient."
  });
});
  }

})

};
// Check if the recipient existe by its mail and password 
exports.login =(req,res) => {
    Recipient.findOne({email:req.params.email,password:req.params.pass})
    .then(recipient => {
        res.send(recipient);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving the recipient."
        });
    });
};
// Check if the recipient existe by its mail  
exports.findOneByMail =(req,res) => {
    Recipient.findOne({email:req.params.mail})
    .then(recipient => {
        res.send(recipient);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving the recipient."
        });
    });
};


// Retrieve and return all recipient from the database.
exports.findAll = (req, res) => {
    Recipient.find()
    .then(recipient => {
        res.send(recipient);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving recipient."
        });
    });
};

// Find a single recipient with a recipient's id
exports.findOne = (req, res) => {
    Recipient.findById(req.params.recipientId)
    .then(recipient => {
        if(!recipient) {
            return res.status(404).send({
                message: "Recipient not found with id " + req.params.recipientId
            });            
        }
        res.send(recipient);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Recipient not found with id " + req.params.recipientId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Recipient with id " + req.params.recipientId
        });
    });
};

// Update a recipient identified by the recipientId in the request
exports.update = (req, res) => {
        // Validate request
        const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

        if (!errors.isEmpty()) {
          res.status(422).json({ errors: errors.array() });
          return;
        }
    if(!req.body) {
        return res.status(400).send({
            message: "can not be empty"
        });
    }

    // Find recipient and update it with the request body
    Recipient.findByIdAndUpdate(req.params.recipientId, {
        firstName: req.body.firstName || null,
        lastName: req.body.lastName || null,
        phoneNumber: req.body.phoneNumber || null,
        address: req.body.address || null,
        zone: req.body.zone || "Autres",
        email: req.body.email || null,
        password:req.body.password || null,
        connectionInfo:req.body.connectionInfo || {connected:false,connectionDate:new Date(),connectionSource:"none"},
        token: Math.floor(Math.random() * (99999-10000)+10000)
    }, {new: true})
    .then(recipient => {
        if(!recipient) {
            return res.status(404).send({
                message: "Recipient not found with id " + req.params.recipientId
            });
        }
        res.send(recipient);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Recipient not found with id " + req.params.recipientId
            });                
        }
        return res.status(500).send({
            message: "Error updating Recipient with id " + req.params.recipientId
        });
    });
};


// Delete a recipient with the specified recipientId in the request
exports.delete = (req, res) => {
    Recipient.findByIdAndRemove(req.params.recipientId)
    .then(recipient => {
        if(!recipient) {
            return res.status(404).send({
                message: "Recipient not found with id " + req.params.recipientId
            });
        }
        res.send({message: "Recipient deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Recipient not found with id " + req.params.recipientId
            });                
        }
        return res.status(500).send({
            message: "Could not delete recipient with id " + req.params.recipientId
        });
    });
};
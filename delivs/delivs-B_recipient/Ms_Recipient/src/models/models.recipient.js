const mongoose = require('mongoose');
var mongooseToCsv = require('mongoose-to-csv');

const RecipientSchema = mongoose.Schema({

    firstName:      String,
    lastName:      String,
	phoneNumber:   String,
	address: String,
	coordinates:{
		latitude:String,
		longitude:String
	},
	email: String,
	connectionInfo: {
		connected:Boolean,
		connectionDate:Date,
		connectionSource:String
	},
	password: String,
	verified: Boolean,
	type:String,
	zone:String,
	token:String,
}, {
    timestamps: true
});
RecipientSchema.plugin(mongooseToCsv, {
	delimiter : ';',
	headers: '_id firstName lastName phoneNumber address zone connectionInfo_connected connectionInfo_connectionDate connectionInfo_connectionSource email type verified createdAt updatedAt',
	virtuals: {
		'connectionInfo_connected': function(doc) {
			if(!doc.connectionInfo){
				return 0;
			}
			return doc.connectionInfo.connected;
		},
		'connectionInfo_connectionDate': function(doc) {
			if(!doc.connectionInfo){
				return 0;
			}
			let d = new Date(doc.connectionInfo.connectionDate);
			return d.getDate()+'/'+(d.getMonth()+1)+'/'+d.getFullYear()+" "+d.getHours()+':'+d.getMinutes()+':'+d.getSeconds();;
		},
		'connectionInfo_connectionSource': function(doc) {
			if(!doc.connectionInfo){
				return 0;
			}
			return doc.connectionInfo.connectionSource;
		},
		'createdAt': function(doc) {
		  return new Date(doc.createdAt).getDate()+'/'+(new Date(doc.createdAt).getMonth()+1)+'/'+new Date(doc.createdAt).getFullYear();
	  },
	  'updatedAt': function(doc) {
		  return new Date(doc.updatedAt).getDate()+'/'+(new Date(doc.updatedAt).getMonth()+1)+'/'+new Date(doc.updatedAt).getFullYear();
	}
	  }
	
  });
module.exports = mongoose.model('Recipient', RecipientSchema);
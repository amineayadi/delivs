module.exports = (app) => {
    const recipient = require('../controllers/controllers.recipient');

      // export data CSV
      app.get('/exportData', recipient.exportData);
     // sendemail
     app.post('/confirmMail', recipient.confirmMail);
    // Create a new recipient
    app.post('/recipient',recipient.validate('create'), recipient.create);
    // Create a new recipient social account
    app.post('/recipientSocailAccount', recipient.createSocial);
    // Login
    app.get('/login/:email/:pass', recipient.login);

    // Retrieve all recipient
    app.get('/recipient', recipient.findAll);

    // Retrieve a single recipient with mail
    app.get('/getByMail/:mail', recipient.findOneByMail);

    // Retrieve a single recipient with recipient
    app.get('/recipient/:recipientId', recipient.findOne);

    // Update a sender with recipient
    app.put('/recipient/:recipientId',recipient.validate('update'), recipient.update);

    // Delete a sender with recipient
    app.delete('/recipient/:recipientId', recipient.delete);
}
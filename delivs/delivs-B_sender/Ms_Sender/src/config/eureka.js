const Eureka = require('eureka-js-client').Eureka;

const client = new Eureka({
  instance: {
    instanceId: 'SENDERSERVICE',
    app: 'SENDERSERVICE',
    hostName: 'delivery-ms-sender',
    ipAddr: 'delivery-ms-sender',
    statusPageUrl: 'http://delivery-ms-sender:3000',
    port: {
      '$': 3000,
      '@enabled': 'true',
    },
    vipAddress: 'SENDERSERVICE',
    dataCenterInfo: {
      '@class': 'com.netflix.appinfo.InstanceInfo$DefaultDataCenterInfo',
      name: 'MyOwn',
    },
    registerWithEureka: true,
    fetchRegistry: true,
  },
  eureka: {
    host: 'delivery-eureka-service',
    port: 8763,
    servicePath: '/eureka/apps/',
  }
});
client.logger.level('debug');
module.exports = client
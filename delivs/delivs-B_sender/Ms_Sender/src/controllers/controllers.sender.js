const Sender = require('../models/models.sender');
const { body,check, validationResult } = require('express-validator');
var fs = require('fs');
var s3 = require('../models/aws')
var s3_r = require('../config/aws-reporting')
const BUCKET = 'marketplaceupload'
const BUCKET_R = 'delivs-reporting'
const Token = require('../models/token');
const { smtpTransport}= require('../config/mail');
var crypto = require('crypto');

var emailCheck = require('email-check');
exports.validate = (method) => {
    switch (method) {
      case 'create': {
       return [ 
        check('name').exists(),
        check('email').exists().isEmail().withMessage('Invalid email'),
        check('phoneNumber').optional({nullable: true}).isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid phoneNumber'),
        check('address').exists(),
      //  check('rib').optional({nullable: true}).isNumeric().isLength({ min: 20,max:20 }).withMessage('Invalid RIB'),
        check('type').isIn(['0','1']).withMessage('Invalid Type'),
        check('password').isLength({ min: 5 }).withMessage('must be at least 5 chars long').matches(/\d/).withMessage('must contain a number'),    
    ]   
      }
      case 'update': {
        return [ 
         check('name').exists(),
         check('email').exists().isEmail().withMessage('Invalid email'),
         check('phoneNumber').optional({nullable: true}).isNumeric().isLength({ min: 8,max:8 }).withMessage('Invalid phoneNumber'),
         check('address').exists(),
        // check('rib').optional({nullable: true}).isNumeric().isLength({ min: 20,max:20 }).withMessage('Invalid RIB'),
         check('type').isIn(['0','1']).withMessage('Invalid Type'),
         check('password').isLength({ min: 5 }).withMessage('must be at least 5 chars long').matches(/\d/).withMessage('must contain a number'),
         body('menu','invalid Menu ').optional({nullable: true}).isArray(),
         body('menu.*.price','Invalid Price').optional().isFloat()    
     ]   
       }
    }
  }

  exports.exportData = (req, res) => {
    const filename = 'delivs-sender-input.csv'
    const storage = 'uploads/'
    Sender.find({type:"0"}).stream().pipe(Sender.csvTransformStream())
    .pipe(fs.createWriteStream(storage+filename)).on('finish', function() {
        s3_r.putObject({
            Bucket: BUCKET_R,
            Body: fs.readFileSync(storage+filename),
            Key: filename,
            ACL:'public-read',
            Expires:new Date(2040,12,31,0,0,0,0)
          })
            .promise()
            .then(response => { res.send(`${s3_r.getSignedUrl('getObject', { Bucket: BUCKET_R, Key: filename })}`.split("?").shift().toString())
            })
            .catch(err => {
                res.send(false)
              console.log('failed:', err)
            })
    })

   

};
  exports.check = (req, res) => {
    emailCheck(req.params.email)
    .then(resp => {
      console.log('res: '+resp);
     if(resp){  res.status(200).send({
        message: true
    });}
     if(!resp){
        res.status(200).send({
            error:true
        });}
       
    }).catch( err => {

        if (err.message === 'refuse') {
 
            res.status(200).send({
                error:true
            });
        } else {
            res.status(200).send({
                error:true
            });
        }
      });
  }
// Upload Image
exports.upload = (req, res) => {

    if(!req.body) {
        return res.status(400).send({
            message: "Body can not be empty"
        });
    }
    
    s3.putObject({
        Bucket: BUCKET,
        Body: fs.readFileSync(req.file.destination+req.file.filename),
        Key: req.file.filename,
        ACL:'public-read',
        Expires:new Date(2040,12,31,0,0,0,0)
      })
        .promise()
        .then(response => { res.send(`${s3.getSignedUrl('getObject', { Bucket: BUCKET, Key: req.file.filename })}`.split("?").shift().toString())
        })
        .catch(err => {
          console.log('failed:', err)
        })

   

};
  // Confirm mail
  exports.send = (req, res) => {
    const mailOptions = {
        from: "contact@delivs.tn",
        to:req.body.mail,
        subject: "Nouveau Mot de passe ",
        generateTextFromHTML: true,
        html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
        '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
        '<h3 style="margin-top:5%;color:#6861e1">Bonjour '+req.body.name+'</h3>'+
        '<p>Votre nouveau mot de passe est '+req.body.password+'</p>'+
        '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
                           
    };
    smtpTransport.sendMail(mailOptions, (error, response) => {
        smtpTransport.close();
    })

};
  // Confirm mail
  exports.confirmMail = (req, res) => {


    // Find a matching token
    Token.findOne({ token: req.body.token }).then(token=>{
        if(token){
            Sender.findOne({ _id: token._userId }).then(sender=>{
                var verified = sender.verified;
                sender.verified=true;
                sender.save().then(data=>{
                    if(verified){res.send(data);}
                    else {
                        const mailOptions = {
                            from: "contact@delivs.tn",
                            to: sender.email,
                            subject: "Bienvenue",
                            generateTextFromHTML: true,
                            html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
                            '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
                            '<h3 style="margin-top:5%;color:#6861e1">Bonjour '+sender.name+'</h3>'+
                            '<p>votre compte a été activé avec succès</p>'+
                            '<p>vous pouvez maintenant se connecter et continuer de saisir vos informations </p>'+
                            '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
                                               
                        };
                        smtpTransport.sendMail(mailOptions, (error, response) => {
                            res.send(data);
                            smtpTransport.close();
                        })   
                    }
                }).catch(err=>{
                    res.status(500).send({
                        message: err.message || "Some error occurred while updating the sender."
                    });
                })
            }).catch(err=>{return res.status(400).send({ msg: 'We were unable to find a user for this token.' });})
        }
        else {
            Sender.findOne({email:req.body.email})
            .then(sender => {
                var token = new Token({ _userId: sender._id, token: crypto.randomBytes(16).toString('hex') });
                token.save().then(tokenObject=>{
                    const mailOptions = {
                    from: "contact@delivs.tn",
                    to: sender.email,
                    subject: "Confirmation ",
                    generateTextFromHTML: true,
                    html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
                    '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
                    '<h3 style="margin-top:5%;color:#6861e1">Bonjour '+sender.name+'</h3>'+
                    '<p>Le lien d\'activation a été expiré</p>'+
                    '<p>Pour activer votre compte, veuillez cliquer sur le lien ci dessous </p>'+
                    '<p><a href="' + req.body.url + '/'+tokenObject.token+'/'+sender.email+'"> Lien d\'activation </a></p>'+
                    '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
                
                    
                };
      
            smtpTransport.sendMail(mailOptions, (error, response) => {
                res.send(false);
                smtpTransport.close();
            })
        }).catch(err => {
            res.status(500).send({
                message: err.message || "Some error occurred while creating the Token."
            });
        })
                
                
            }).catch(err => {
                res.status(500).send({
                    message: err.message || "Some error occurred while retrieving Authentification."
                });
            })
        }
    }).catch(err=>{
        return res.status(400).send({ type: 'not-verified', msg: 'We were unable to find a valid token. Your token my have expired.' });
    })
};
// Create and Save a new Sender
exports.create = (req, res) => {
    // Validate request
    const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

      if (!errors.isEmpty()) {
        res.status(422).json({ errors: errors.array() });
        return;
      }
    if(!req.body) {
        return res.status(400).send({
            message: "Sender can not be empty"
        });
    }

               // Create a Sender
               const sender = new Sender({
     
                name: req.body.name || null,
                lastName: req.body.lastName ,
                phoneNumber: req.body.phoneNumber ||null,
                address: req.body.address || null,
                zone: req.body.zone || "Autres",
                email: req.body.email ||null,
                password:req.body.password || null,
                banner:req.body.banner|| null,
                type:req.body.type,
                menu:req.body.menu || [],
                // verified: req.body.verified,
                verified: true,
                active: req.body.active || false,
                coordinates: req.body.coordinates || null,
                minOrder: req.body.minOrder || 0,
                delivsFee:req.body.delivsFee ||   { typeFee: 0, method: 0, fee: 0 },
                token: Math.floor(Math.random() * (99999-10000)+10000)
            });
            // check email is unique
            Sender.findOne({email:sender.email})
            .then(senders => {
                if(senders){
                    //Email exists
                    res.send(true);
                }
                else {
                     // Save Sender in the database
                    sender.save()
                    .then(data => {
                        res.send(data) ; 
             /*              // Create a verification token for this user
                var token = new Token({ _userId: data._id, token: crypto.randomBytes(16).toString('hex') });
                token.save().then(tokenObject=>{
                  const mailOptions = {
                        from: "contact@delivs.tn",
                        to: req.body.email,
                        subject: "Activer votre compte restaurant Deliv's",
                        generateTextFromHTML: true,
                        html:'<div style="max-width: 1000;margin:auto;text-align:center" >'+
                        '<img src="https://marketplaceupload.s3.eu-west-3.amazonaws.com/logo-delivs.png" >'+
                        '<h3 style="margin-top:5%;color:#6861e1">Activer votre compte Deliv\'s</h3>'+
                        '<p>Pour activer votre compte, veuillez cliquer sur le lien ci dessous </p>'+
                        '<p><a href="' + req.body.url + '/'+tokenObject.token+'/'+data.email+'"> Lien d\'activation </a></p>'+
                        '<hr style="width: 50%;"><p>Ceci est un mail automatique, Merci de ne pas y répondre.</p></div>',
                    
                        
                    };
                     smtpTransport.sendMail(mailOptions, (error, response) => {
                        smtpTransport.close();

                    }) ; 
                   
                }).catch(err => {
                    res.status(500).send({
                        message: err.message || "Some error occurred while creating the Token."
                    });
                })
                */
            }).catch(err => {
                        res.status(500).send({
                            message: err.message || "Some error occurred while creating the Sender."
                        });
                    });
                }
            
            })
    



    

    



   

};

// Check if the sender existe by its mail and password 
exports.login =(req,res) => {
    Sender.findOne({email:req.params.email,password:req.params.pass})
    .then(senders => {
        res.send(senders);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving the sender."
        });
    });
};
// Check if the sender existe by its mail  
exports.findOneByMail =(req,res) => {
    Sender.findOne({email:req.params.mail})
    .then(senders => {
        res.send(senders);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving the sender."
        });
    });
};

// Retrieve and return all sender from the database.
exports.findAll = (req, res) => {
    Sender.find({type:"0"})
    .then(senders => {
        res.send(senders);
    }).catch(err => {
        res.status(500).send({
            message: err.message || "Some error occurred while retrieving senders."
        });
    });
};

// Find a single sender with a sender's id
exports.findOne = (req, res) => {
    Sender.findById(req.params.senderId)
    .then(sender => {
        if(!sender) {
            return res.status(404).send({
                message: "Sender not found with id " + req.params.senderId
            });            
        }
        res.send(sender);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Sender not found with id " + req.params.senderId
            });                
        }
        return res.status(500).send({
            message: "Error retrieving Sender with id " + req.params.senderId
        });
    });
};

// Update a sender identified by the senderId in the request
exports.update = (req, res) => {
        // Validate request
        const errors = validationResult(req); // Finds the validation errors in this request and wraps them in an object with handy functions

        if (!errors.isEmpty()) {
          res.status(422).json({ errors: errors.array() });
          return;
        }
    if(!req.body) {
        return res.status(400).send({
            message: "can not be empty"
        });
    }

    // Find sender and update it with the request body
    Sender.findByIdAndUpdate(req.params.senderId, {
        name: req.body.name || null,
        lastName: req.body.lastName ,
        phoneNumber: req.body.phoneNumber || null,
        address: req.body.address || null,
        email: req.body.email || null,
        password:req.body.password || null,
        zone: req.body.zone || "Autres",
        banner:req.body.banner || null,
	    type:req.body.type,
        menu:req.body.menu || [],
        times:req.body.times || [],
        closed:req.body.closed,
        active: req.body.active || false,
        minOrder: req.body.minOrder || null,
        delivsFee:req.body.delivsFee ||  { typeFee: 0, method: 0, fee: 0 },
        coordinates: req.body.coordinates || null,
        token: req.body.token 
    }, {new: true})
    .then(sender => {
        if(!sender) {
            return res.status(404).send({
                message: "Sender not found with id " + req.params.senderId
            });
        }
        res.send(sender);
    }).catch(err => {
        if(err.kind === 'ObjectId') {
            return res.status(404).send({
                message: "Sender not found with id " + req.params.senderId
            });                
        }
        return res.status(500).send({
            message: "Error updating sender with id " + req.params.senderId
        });
    });
};


// Delete a sender with the specified senderId in the request
exports.delete = (req, res) => {
    Sender.findByIdAndRemove(req.params.senderId)
    .then(sender => {
        if(!sender) {
            return res.status(404).send({
                message: "Sender not found with id " + req.params.senderId
            });
        }
        res.send({message: "Sender deleted successfully!"});
    }).catch(err => {
        if(err.kind === 'ObjectId' || err.name === 'NotFound') {
            return res.status(404).send({
                message: "Sender not found with id " + req.params.senderId
            });                
        }
        return res.status(500).send({
            message: "Could not delete sender with id " + req.params.senderId
        });
    });
};
const mongoose = require('mongoose');
var mongooseToCsv = require('mongoose-to-csv');
const SenderSchema = mongoose.Schema({
	token:String,
    name:      String,
    lastName:      String,
	phoneNumber:   String,
	cin:   String,
	address: String,
	coordinates:{
		latitude:String,
		longitude:String
	},
	zone:String,
	delivsFee:{
		typeFee:Number,
		fee:Number,
		method:Number,
	},
	rib:   String,
	email: String,
	password: String,
	banner:String,
	type:String,
	verified: Boolean,
	closed:Boolean,
	minOrder:Number,
	active:Boolean,
	menu:[
		{
			name:String,
			description:String,
			category:String,
			price:Number,
			available:Boolean,
			dayAvailable:Number,
			image:String,
			options : [
				{
					optionName:String,
					comment:String,
					buttonType :Boolean,
					optionLines : [
						{
							name:String,
							price:Number
						}
					]
				}
			]
		}
	],
	times:[
		{   Day:Number,
			DayofTheWeek:String,
			openningTime:String,
			closingTime:String,
			closed:Boolean,
			
		}
	]
	
}, {
    timestamps: true
});
SenderSchema.plugin(mongooseToCsv, {
	delimiter : ';',
	headers: '_id name phoneNumber address zone coordinates_latitude coordinates_longitude delivsFee_typeFee delivsFee_fee delivsFee_method email banner type verified closed minOrder active createdAt updatedAt',
	virtuals: {
		'coordinates_latitude': function(doc) {
		  return doc.coordinates.latitude;
		},
		'coordinates_longitude': function(doc) {
			return doc.coordinates.longitude;
		}
		,
		'delivsFee_typeFee': function(doc) {
			if(!doc.delivsFee){
				return 0;
			}
			return doc.delivsFee.typeFee;
		},
		'delivsFee_fee': function(doc) {
			if(!doc.delivsFee){
				return 0;
			}
			return doc.delivsFee.fee;
		},
		'delivsFee_method': function(doc) {
			if(!doc.delivsFee){
				return 0;
			}
			return doc.delivsFee.method;
		},
		'createdAt': function(doc) {
		  return new Date(doc.createdAt).getDate()+'/'+(new Date(doc.createdAt).getMonth()+1)+'/'+new Date(doc.createdAt).getFullYear();
	  },
	  'updatedAt': function(doc) {
		  return new Date(doc.updatedAt).getDate()+'/'+(new Date(doc.updatedAt).getMonth()+1)+'/'+new Date(doc.updatedAt).getFullYear();
	}
	  }
	
  });
module.exports = mongoose.model('Sender', SenderSchema);
module.exports = (app) => {
    const sender = require('../controllers/controllers.sender.js');
    const upload = require('../models/upload')
    
    // export data CSV
    app.get('/exportData', sender.exportData);

    app.post('/sendMail',sender.send);
    // Upload an image
    app.get('/checkMail',sender.check);
    // Upload an image
    app.post('/upload',upload.single('image'),sender.upload);
    // Create a new Sender
    app.post('/sender',sender.validate('create'),sender.create);
     // sendemail
     app.post('/confirmMail', sender.confirmMail);
    // Login
    app.get('/login/:email/:pass', sender.login);

    // Retrieve allt Senders
    app.get('/sender', sender.findAll);

    // Retrieve a single sender with mail
    app.get('/getByMail/:mail', sender.findOneByMail);

    // Retrieve a single sender with senderId
    app.get('/sender/:senderId', sender.findOne);

    // Update a sender with senderId
    app.put('/sender/:senderId',sender.validate('update'), sender.update);

    // Delete a sender with senderId
    app.delete('/sender/:senderId', sender.delete);
}

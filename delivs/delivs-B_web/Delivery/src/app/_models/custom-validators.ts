import { Directive, Input, OnChanges, SimpleChanges } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator, ValidatorFn, Validators } from '@angular/forms';

/** A hero's name can't match the given regular expression */
export function passwordValidator(): ValidatorFn {
  return (control: AbstractControl): {[key: string]: any} => {
    let regNumber: RegExp = new RegExp(/\d/)
    let regString:RegExp =new RegExp(/[a-zA-Z]/)
    const valid = regNumber.test(control.value) && regString.test(control.value);
    return !valid ? {'password': {value: control.value}} : null;
  };
}

@Directive({
  selector: '[passwordValidator]',
  providers: [{provide: NG_VALIDATORS, useExisting: PasswordValidatorDirective, multi: true}]
})
export class PasswordValidatorDirective implements Validator {
  @Input('passwordValidator') passwordValidator: string;

  validate(control: AbstractControl): {[key: string]: any} {
    return this.passwordValidator ? passwordValidator()(control)
                              : null;
  }
}

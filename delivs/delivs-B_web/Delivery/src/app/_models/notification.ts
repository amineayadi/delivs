export class Notification {
    notification:{
        title:string;
        body:string;
        sound:string;
        click_action:string;
        icon:string;
    };
    data:{
        landing_page:string;
    };
    to:string;
    priority:string;
    restricted_package_name:string;
    public constructor(title,body,page,to) {
        this.notification= {
            title:title,
            body:body,
            sound:"default",
            click_action:"FCM_PLUGIN_ACTIVITY",
            icon:"fcm_push_icon"
        }
        this.data={
            landing_page:page
        }
        this.to="/topics/"+to;
        this.priority="high";
        this.restricted_package_name="";
    }
    

}

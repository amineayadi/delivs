import { OrderItem } from './orderItem';


export class Order {
    _id:string;
    total:number;
    totalDelivs:number;
    customer:string;
    resto:string;
    orderItems:OrderItem[];
    public constructor(init?: Partial<Order >) {
        this.orderItems=[];
        Object.assign(this, init);
    }
}
export class Recipient {
    _id:string;
    firstName:string;
    lastName:string;
    phoneNumber:string;
    cin:string;
    address:string;
    rib:string;
    email:string;
    password:string;
    verified:boolean;
    type:string;
    token:string;

    connectionInfo:ConnectionInfo;
    public constructor(init?: Partial<Recipient >) {
        Object.assign(this, init);
    }

}
export class ConnectionInfo {
    connected:boolean;
    connectionDate:Date;
    connectionSource:String;
     public constructor(connected){
         this.connected=connected;
         this.connectionSource="Web";
         this.connectionDate = new Date();
     }
}
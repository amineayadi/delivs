import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  Delivery } from 'src/app/_models/delivery';
import { SenderLoginService } from '../sender/sender-login.service';
import { RecipientLoginService } from '../recipient/recipient-login.service';
import { DeliveryManLoginService } from '../deliveryMan/delivery-login.service';
import {Notification} from './../../_models/notification';

@Injectable({
  providedIn: 'root'
})
export class DeliveryService {
  env : Env = new Env();
  private Url = this.env.host+'/deliveryservice/';
  constructor(private http: HttpClient,private loginService:SenderLoginService,private recipientLoginService:RecipientLoginService,
    private deliverymanLoginService:DeliveryManLoginService) { }
       /** POST: send a new notif  */
   sendNotification (notif: Notification): Observable<any> {
    let headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'Authorization': 'key=AAAA3_u_sp4:APA91bGI-lUZGgnJGUB-BO7GivRMR5ZxEfcfzOCEMrFW4CMxnwFWOn5a6Qf2JKcuQZ_2moWM36DY1N3aGp3T-OSK_C9Ys57oa4QBUxHClzWTIAzK7wzGFJ4VfZX6HQMJNRw1KU4ikHjZ'
    })
    return this.http.post<any>('https://fcm.googleapis.com/fcm/send', notif,{ headers: headers })
      .pipe(
        catchError(this.handleError<any>('sendNotification', null))
      );
  }
         /** POST: add a new Delivery  */
         createDelivery (delivery: Delivery): Observable<Delivery> {
          return this.http.post<Delivery>(this.Url+'delivery', delivery)
            .pipe(
              catchError(this.handleError<Delivery>('createDelivery', delivery))
            );
        }
             /** PUT: update Delivery   */
          updateDelivery (delivery: Delivery): Observable<Delivery> {
              return this.http.put<Delivery>(this.Url+'delivery/'+delivery._id, delivery)
                .pipe(
                  catchError(this.handleError<Delivery>('updateDelivery', delivery))
                );
            }
               /** GET: get one Delivery   */
          getDelivery (id: string): Observable<Delivery> {
            return this.http.get<Delivery>(this.Url+'delivery/'+id)
              .pipe(
                catchError(this.handleError<Delivery>('getDelivery', null))
              );
          }
                /** GET: get all Deliverys   */
          getAllDeliverys (): Observable<Delivery[]> {
            return this.http.get<Delivery[]>(this.Url+'delivery')
              .pipe(
                catchError(this.handleError<Delivery[]>('getAllDeliverys', []))
              );
          }
          /** GET:  Retrieve all deliveries belonging to a sender  */
          getDeliveryBySender (idSender): Observable<Delivery[]> {
         return this.http.get<Delivery[]>(this.Url+'deliveryBySender/'+idSender)
          .pipe(
           catchError(this.handleError<Delivery[]>('getDeliveryBySender', null))
            );
          }
          /** GET: Retrieve all deliveries belonging to a deliveryMan*/
          getDeliveryByDelievryMan (idDeliveryMan): Observable<Delivery[]> {
            return this.http.get<Delivery[]>(this.Url+'deliveryByDeliveryMan/'+idDeliveryMan)
             .pipe(
              catchError(this.handleError<Delivery[]>('getDeliveryByDelievryMan', null))
               );
             }
           /** GET: Retrieve all deliveries belonging to a deliveryMan with specific status*/
           getDeliveryByDelievryManAndStatus (id,status): Observable<Delivery[]> {
            return this.http.get<Delivery[]>(this.Url+'deliveryByStatusAndDeliveryman/'+id+'/'+status)
             .pipe(
              catchError(this.handleError<Delivery[]>('getDeliveryByDelievryManAndStatus', null))
               );
             }
             /** GET: Retrieve all deliveries belonging to a sender with specific status*/
           getDeliveryBySenderAndStatus (status): Observable<Delivery[]> {
           let  idSender = this.loginService.getSender();
            return this.http.get<Delivery[]>(this.Url+'deliveryByStatusAndSender/'+idSender+'/'+status)
             .pipe(
              catchError(this.handleError<Delivery[]>('getDeliveryBySenderAndStatus', null))
               );
             }
            /** GET: Retrieve all deliveries belonging to a Recipient with specific status*/
            getDeliveryByRecipientAndStatus (status): Observable<Delivery[]> {
              let  idRecipient= this.recipientLoginService.getRecipient();
               return this.http.get<Delivery[]>(this.Url+'deliveryByStatusAndRecipient/'+idRecipient+'/'+status)
                .pipe(
                 catchError(this.handleError<Delivery[]>('getDeliveryByRecipientAndStatus', null))
                  );
                }
          /** GET: Retrieve all deliveries belonging to a status*/
          getDeliveryByStatus (status): Observable<Delivery[]> {
            return this.http.get<Delivery[]>(this.Url+'deliveryByStatus/'+status)
             .pipe(
              catchError(this.handleError<Delivery[]>('getDeliveryByStatus', null))
               );
             }
               /** DELETE: delete one Delivery   */
         deleteDelivery (id: string): Observable<Delivery> {
        return this.http.delete<Delivery>(this.Url+'delivery/'+id)
          .pipe(
        catchError(this.handleError<Delivery>('deleteDelivery', null))
         );
              }
          private handleError<T> (operation = 'operation', result?: T) {
            return (error: any): Observable<T> => {
         
              // TODO: send the error to remote logging infrastructure
           //   console.error(error); // log to console instead
         
              // TODO: better job of transforming error for user consumption
            //  console.log(`${operation} failed: ${error.message}`);
         
              // Let the app keep running by returning an empty result.
              return of(result as T);
            };
          }
}

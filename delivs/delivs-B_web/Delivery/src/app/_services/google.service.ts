import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class GoogleService {

 
 Url = "https://www.googleapis.com/geolocation/v1/geolocate?key=AIzaSyA7WoC8kDd-46mQXoaBj2NTRMDZ-xEAkp8";

  constructor(private http: HttpClient) { }
          /** POST: get lcoation  */
       getLocation (): Observable<any> {
          return this.http.post<any>(this.Url,{})
            .pipe(
              catchError(this.handleError<any>('getLocation', null))
            );
        }


     /*    getDistanceTime(): Observable<any> {
          return this.http.post<any>("https://www.googleapis.com/distancematrix/json?origins=Boston,MA|Charlestown,MA&destinations=Lexington,MA|Concord,MA&departure_time=now&key=AIzaSyB_x0SHuUEolw4wmj-u3P93Eco0AmZFtcA",{})
          .pipe(
            catchError(this.handleError<any>('getDistanceTime', null))
          )
        } */

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead
 
      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

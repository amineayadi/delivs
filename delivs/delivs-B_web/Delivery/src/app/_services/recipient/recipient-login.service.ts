import { Injectable } from '@angular/core';
import SimpleCrypto from "simple-crypto-js";
import { Router } from '@angular/router';
import {Location} from '@angular/common';
import { AuthService } from 'angularx-social-login';
@Injectable({
  providedIn: 'root'
})
export class RecipientLoginService {
  _secretKey = "Five12345";
  simpleCrypto = new SimpleCrypto(this._secretKey);
  constructor(private _location: Location,private router : Router,private authService: AuthService) { }
  encrypt(object){

    return  this.simpleCrypto.encrypt(object);
  }
  decrypt(object):any{
    return this.simpleCrypto.decrypt(object); 
  }
  login(id:string,login:Boolean){

      sessionStorage.setItem('isLoggedInR','true');
      sessionStorage.setItem('recipient',this.encrypt(id));
        (!login)?sessionStorage.setItem('social','true'):"";
        this._location.back()
      
  }

  getRecipient():string{
    let recipientId:string;

    recipientId =sessionStorage.getItem('recipient')     
  
    if(recipientId){ return this.decrypt(recipientId);}
      else{return null;}
    
  }
  isLoggedIn():Boolean{

        return Boolean(JSON.parse(sessionStorage.getItem('isLoggedInR'))); 

  }
  isSocial():Boolean{

    return Boolean(JSON.parse(sessionStorage.getItem('social'))); 

}
  logout() {

        sessionStorage.removeItem('isLoggedInR');
        sessionStorage.removeItem('recipient');
        if(this.isSocial()){
          sessionStorage.removeItem('social');
          this.authService.signOut();
        }
      this.router.navigate(["login"]);
  }





}

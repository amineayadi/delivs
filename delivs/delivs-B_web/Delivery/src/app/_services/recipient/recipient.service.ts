import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  Recipient } from 'src/app/_models/recipient';

@Injectable({
  providedIn: 'root'
})
export class RecipientService {

  env : Env = new Env();
  private Url = this.env.host+'/recipientservice/';
  path=location.origin+"/confirmation"
  constructor(private http: HttpClient) { }
    /** GET: ByMail  */
    getRecipientByMail (mail): Observable<Recipient> {
      return this.http.get<Recipient>(this.Url+'getByMail/'+mail)
       .pipe(
        catchError(this.handleError<Recipient>('getRecipientByMail', null))
         );
       }
         /** GET: checkMail */
         checkMail(key,mail): Observable<any> {
      
          return this.http.get<any>("https://api.email-validator.net/api/verify?APIKey="+key+"&EmailAddress="+mail)
          .pipe(
            catchError(err => {
              // console.log('caught mapping error and rethrowing', err);
              return throwError(err );
          })
          )
       
    
     }
          /** POST: Confirm mail  */
       confirmRecipientMail (token: string,mail:string): Observable<Recipient> {
      
        return this.http.post<Recipient>(this.Url+'confirmMail', {'token':token,'email':mail,'url':this.path})
          .pipe(
            catchError(this.handleError<Recipient>('confirmRecipientMail', null))
          );
      }
     /** POST: add a new Recipient  */
     createRecipient (recipient: any): Observable<Recipient> {
    
    recipient.url=this.path as any
      return this.http.post<Recipient>(this.Url+'recipient', recipient)
        .pipe(
          catchError(this.handleError<Recipient>('createRecipient', recipient))
        );
    }
     /** POST: add a new Recipient  */
     createRecipientSocial (recipient: any): Observable<Recipient> {
        return this.http.post<Recipient>(this.Url+'recipientSocailAccount', recipient)
          .pipe(
            catchError(this.handleError<Recipient>('createRecipientSocial', recipient))
          );
      }
         /** PUT: update Recipient   */
      updateRecipient (recipient: Recipient): Observable<Recipient> {
          return this.http.put<Recipient>(this.Url+'recipient/'+recipient._id, recipient)
            .pipe(
              catchError(this.handleError<Recipient>('updateRecipient', recipient))
            );
        }
           /** GET: get one Recipient   */
      getRecipient (id: string): Observable<Recipient> {
        return this.http.get<Recipient>(this.Url+'recipient/'+id)
          .pipe(
            catchError(this.handleError<Recipient>('getRecipient', null))
          );
      }
            /** GET: get all Recipients   */
      getAllRecipients (): Observable<Recipient[]> {
        return this.http.get<Recipient[]>(this.Url+'recipient')
          .pipe(
            catchError(this.handleError<Recipient[]>('getAllRecipients', []))
          );
      }
      /** GET: Login  */
     getLoginRecipient (mail,password): Observable<Recipient> {
     return this.http.get<Recipient>(this.Url+'login/'+mail+'/'+password)
      .pipe(
       catchError(this.handleError<Recipient>('getLoginRecipient', null))
        );
      }
           /** DELETE: delete one Recipient   */
     deleteRecipient (id: string): Observable<Recipient> {
    return this.http.delete<Recipient>(this.Url+'recipient/'+id)
      .pipe(
    catchError(this.handleError<Recipient>('deleteRecipient', null))
     );
          }
      private handleError<T> (operation = 'operation', result?: T) {
        return (error: any): Observable<T> => {
     
          // TODO: send the error to remote logging infrastructure
          // console.error(error); // log to // console instead
     
          // TODO: better job of transforming error for user consumption
          // console.log(`${operation} failed: ${error.message}`);
     
          // Let the app keep running by returning an empty result.
          return of(result as T);
        };
      }

}
  
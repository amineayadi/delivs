import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  Sender } from 'src/app/_models/sender';
import { DeliveryFeeService } from '../deliveryFee/delivery-fee.service';

@Injectable({
  providedIn: 'root'
})
export class SenderService {

  env : Env = new Env();
  private Url = this.env.host+'/senderservice/';
  path=location.origin+"/restaurant/confirmation"
  constructor(private http: HttpClient) { }
  /** GET: ByMail  */
  getSenderByMail (mail): Observable<Sender> {
    return this.http.get<Sender>(this.Url+'getByMail/'+mail)
     .pipe(
      catchError(this.handleError<Sender>('getSenderByMail', null))
       );
     }
     /** GET: checkMail */
     checkMail(key,mail): Observable<any> {
      
         return this.http.get<any>("https://api.email-validator.net/api/verify?APIKey="+key+"&EmailAddress="+mail)
         .pipe(
           catchError(err => {
             // console.log('caught mapping error and rethrowing', err);
             return throwError(err );
         })
         )
      
   
    }
     /** POST: uploadImage */
    uploadImage(image: any): Observable<any> {
      const formdata: FormData = new FormData();
      
        formdata.append('image', image);
        return this.http.post<any>(this.Url+'upload', formdata,{responseType:"text" as "json"})
    .pipe(
      catchError(err => {
        // console.log('caught mapping error and rethrowing', err);
        return throwError(err );
    })
    )
   
    }
     /** POST: add a new Sender  */
createSender (sender: any): Observable<any> {
/*   const formdata: FormData = new FormData();
  for ( var key in sender ) {
    formdata.append(key, sender[key]);
}
formdata.append('url',this.path) */
sender.url  = this.path as any
  return this.http.post<Sender>(this.Url+'sender', sender)
  .pipe(
    catchError(this.handleError<Sender>('createSender', sender))
);
   
    
}
     /** POST: add a new Sender  */
     send (password,mail,name?): Observable<any> {
    
        return this.http.post<any>(this.Url+'sendMail', {
          "mail":mail,
          "password":password,
          "name":name || "",
        })
        .pipe(
          catchError(this.handleError<any>('send', null))
      );
         
          
      }
        /** POST: Confirm mail  */
       confirmSenderMail (token: string,mail:string): Observable<Sender> {
      
        return this.http.post<Sender>(this.Url+'confirmMail', {'token':token,'email':mail,'url':this.path})
          .pipe(
            catchError(this.handleError<Sender>('confirmDeliveryManMail', null))
          );
      }
     /** PUT: update Sender   */
  updateSender (sender: Sender): Observable<Sender> {
      return this.http.put<Sender>(this.Url+'sender/'+sender._id, sender)
        .pipe(
          catchError(this.handleError<Sender>('updateSender', sender))
        );
    }
       /** GET: get one Sender   */
  getSender (id: string): Observable<Sender> {
    return this.http.get<Sender>(this.Url+'sender/'+id)
      .pipe(
        catchError(this.handleError<Sender>('getSender', null))
      );
  }
        /** GET: get all Senders   */
  getAllSenders (): Observable<Sender[]> {
    return this.http.get<Sender[]>(this.Url+'sender')
      .pipe(
        catchError(this.handleError<Sender[]>('getAllSenders', []))
      );
  }
  /** GET: Login  */
 getLoginSender (mail,password): Observable<Sender> {
 return this.http.get<Sender>(this.Url+'login/'+mail+'/'+password)
  .pipe(
   catchError(this.handleError<Sender>('getLoginSender', null))
    );
  }
       /** DELETE: delete one Sender   */
 deleteSender (id: string): Observable<Sender> {
return this.http.delete<Sender>(this.Url+'sender/'+id)
  .pipe(
catchError(this.handleError<Sender>('deleteSender', null))
 );
      }
  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
 
      // TODO: send the error to remote logging infrastructure
      // console.error(error); // log to // console instead
 
      // TODO: better job of transforming error for user consumption
      // console.log(`${operation} failed: ${error.message}`);
 
      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of, throwError } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';
import { Env } from '../env';
import {  DeliveryMan } from 'src/app/_models/deliveryMan';
@Injectable({
  providedIn: 'root'
})
export class SmsService {

  env : Env = new Env();
  path=location.origin+"/deliveryman/confirmation"
  private Url = this.env.host+'/deliverymanservice/';
  constructor(private http: HttpClient) { }
       /** GET: checkMail */
        checkSms(to,content): Observable<any> {


      const headers = { 'Authorization': 'Basic aXB2ZTM0OTk6cjZnZ0R3VTU=', 'Content-Type': 'application/json' };
      
    
      
         return this.http.post<any>(location.origin+"/api",{from:"deliv's",to:"00216"+to,content:content}, { headers })
         .pipe(
           catchError(err => {
             // console.log('caught mapping error and rethrowing', err);
             return throwError(err );
         })
         )
      
   
    }

    sendmail(v){
   
      return this.http.post<any>('https://proxy.five-consulting.com/'+"sendMail",v)
    }
       
    
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- #1 import module
import { AdminRoutingModule } from './admin-routing.module';
import { AdminHeaderComponent } from './admin-header/admin-header.component';
import { AdminFooterComponent } from './admin-footer/admin-footer.component';
import { AdminSidebarComponent } from './admin-sidebar/admin-sidebar.component';
import { DeliveryMenListComponent } from './delivery-men-list/delivery-men-list.component';
import { AdminDeliveryManComponent } from './admin-delivery-man/admin-delivery-man.component';
import { FoodDeliveriesComponent } from './food-deliveries/food-deliveries.component';
import { ListRestaurantsComponent } from './list-restaurants/list-restaurants.component';
import { PageRestaurantsComponent } from './page-restaurants/page-restaurants.component';
import { AuthentificationAdminComponent } from './authentification-admin/authentification-admin.component';
import { AjoutRestaurantComponent } from './ajout-restaurant/ajout-restaurant.component';
import { UpdateRestaurantComponent } from './update-restaurant/update-restaurant.component';
import { DeliveryFeeComponent } from './delivery-fee/delivery-fee.component';
import { GoogleModule} from './../google/google.module';
import { RecipientListComponent } from './recipient-list/recipient-list.component';
import { CategoryListComponent } from './category-list/category-list.component'
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import {NgxPaginationModule} from 'ngx-pagination';
import { AdminDashbordComponent } from './admin-dashbord/admin-dashbord.component';
import { AdminDashbord2Component } from './admin-dashbord2/admin-dashbord2.component';
import { AdminRecipientComponent } from './admin-recipient/admin-recipient.component';


@NgModule({
  declarations: [AdminHeaderComponent, AdminFooterComponent, AdminSidebarComponent, DeliveryMenListComponent, AdminDeliveryManComponent, FoodDeliveriesComponent, ListRestaurantsComponent, PageRestaurantsComponent, AuthentificationAdminComponent,AjoutRestaurantComponent, UpdateRestaurantComponent, DeliveryFeeComponent, RecipientListComponent, CategoryListComponent, AdminDashbordComponent, AdminDashbord2Component,AdminRecipientComponent],
  imports: [
    CommonModule,
    GoogleModule,
    AdminRoutingModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    NgxPaginationModule,
    MDBBootstrapModule.forRoot(),
  ],
  exports:[ AdminHeaderComponent,AdminFooterComponent,AdminSidebarComponent ]
})
export class AdminModule { }

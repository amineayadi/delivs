import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Sender } from './../../_models/sender';
import { SenderService } from './../../_services/sender/sender.service';
import { Product } from './../../_models/product';
import { CategoryService } from 'src/app/_services/category/category.service';
import { Category } from 'src/app/_models/category';


@Component({
  selector: 'app-ajout-restaurant',
  templateUrl: './ajout-restaurant.component.html',
  styleUrls: ['./ajout-restaurant.component.scss']
})
export class AjoutRestaurantComponent implements OnInit {
newRestaurant:Sender=new Sender();
selectedFiles: FileList;
invalid=true;
categories:Category[]= []
  constructor(private senderService:SenderService,private categoryService:CategoryService) { }

  ngOnInit() {
    this.newRestaurant.menu=[new Product()];
    this.getCategories();

   }

   getCategories(){
    this.categoryService.getAllcategories().subscribe(categories=>{
      this.categories=categories;
    });
  }
   selectFile(event) {
     this.invalid=false
    this.selectedFiles = event.target.files;
    this.newRestaurant.banner= this.selectedFiles.item(0).name;
  }
  
   addProduct(){
    this.newRestaurant.menu[this.newRestaurant.menu.length]=new Product();
  }
newSender(){
this.newRestaurant.type="0";
this.newRestaurant.banner=this.selectedFiles.item(0);
let menu = this.newRestaurant.menu;
delete this.newRestaurant.menu
 this.senderService.createSender(this.newRestaurant).subscribe(sender=>{
sender.menu = menu ;
this.senderService.updateSender(sender).subscribe(newSender=>{
  this.newRestaurant = new Sender();
})
}); 
}


}

import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Sender } from 'src/app/_models/sender';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';


@Component({
  selector: 'app-authentification-admin',
  templateUrl: './authentification-admin.component.html',
  styleUrls: ['./authentification-admin.component.scss']
})
export class AuthentificationAdminComponent implements OnInit {

  @ViewChild('frame', { static: true }) public formModal;
  signInForm: FormGroup;
  errorMessage=false;


  new:boolean = false;
  constructor(private senderService:SenderService,private loginService:SenderLoginService) { }

  ngOnInit() {
  
       this.signInFormBuild();
  }
  ngAfterViewChecked(){
    this.formModal.show();
  }
  signInFormBuild(){
    this.signInForm =  new FormGroup({
      email: new FormControl('',[ Validators.email, Validators.required]),
      password: new FormControl('', Validators.required)
    });
  }

  get getSignInFormEmail() {
    return this.signInForm.get('email');
  }
  get getSignInFormPassword() {
    return this.signInForm.get('password');
  }

  signin(){
    let sender= new Sender(this.signInForm.value);
    this.senderService.getLoginSender(sender.email,sender.password).subscribe(newsender=>{
      if(newsender!=null){
        this.errorMessage=false;
        if(newsender.type == "admin"){
          this.loginService.loginAdmin(newsender._id)
      
      }
      }else {
        
        this.errorMessage=true;
      }
    
    })
  }



}

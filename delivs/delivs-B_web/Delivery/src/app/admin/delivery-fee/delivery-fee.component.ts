import { Component, OnInit } from '@angular/core';
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';
import { DeliveryFee } from 'src/app/_models/deliveryFee';
@Component({
  selector: 'app-delivery-fee',
  templateUrl: './delivery-fee.component.html',
  styleUrls: ['./delivery-fee.component.scss']
})
export class DeliveryFeeComponent implements OnInit {

  deliveryFeeCount:number;
  delievryFee:DeliveryFee= new DeliveryFee()
  constructor(private deliveryFeeService:DeliveryFeeService) { }

  ngOnInit() {
    this.getDeliveryFee()
  }
  getDeliveryFee(){
    this.deliveryFeeService.getOneDeliveryCount().subscribe(count=>{
      this.deliveryFeeCount=count
      if(count !== 0){
        this.deliveryFeeService.getOneDeliveryFee().subscribe(deliveryfee=>{
          this.delievryFee =deliveryfee ;
        })
      }
    })
  }
  save(){
    this.deliveryFeeService.createDeliveryFee(this.delievryFee).subscribe(newDeliveryFee=>{
      this.getDeliveryFee();});
  }

  update(){
    this.deliveryFeeService.updateDeliveryFee(this.delievryFee).subscribe(newDeliveryFee=>
      {})
  }

}


import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { Delivery } from 'src/app/_models/delivery';
import { DeliveryService } from 'src/app/_services/delivery/delivery.service';
import { OrderService } from 'src/app/_services/order/order.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { MdbTableDirective } from 'angular-bootstrap-md';
@Component({
  selector: 'app-food-deliveries',
  templateUrl: './food-deliveries.component.html',
  styleUrls: ['./food-deliveries.component.scss']
})
export class FoodDeliveriesComponent implements OnInit ,AfterViewInit{
  @ViewChild('frame', { static: true }) public formModal;
  @ViewChild(MdbTableDirective, { static: true }) mdbTable:MdbTableDirective;
  deliveries:Delivery[]=[];
  list=[]
  selectedId:string="";
  deliveryMen:DeliveryMan[]=[]
  valid=false;
  p=1;
  deleteId="";

  constructor(private deliveryService:DeliveryService,private orderService:OrderService,private recipientService:RecipientService,
    private delieveryManService:DeliveryManService,private senderService:SenderService) { }
  ngAfterViewInit(): void {
    setTimeout(() => {

      this.list=this.deliveries
      this.mdbTable.setDataSource(this.list);
    }, 1000);
   
  }

  ngOnInit() {
    this.getDeliveryList();
    this.getDeliveryMenList();
  }
  delete(){
    this.deliveryService.deleteDelivery(this.deleteId).subscribe(data=>{
      this.getDeliveryList();
      this.getDeliveryMenList();
    })
    }
  getDeliveryList(){
    this.deliveryService.getAllDeliverys().subscribe(deliveries => {
      
      this.deliveries=deliveries;
      this.deliveries.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index),this.getSender(index)})
    })
  }
  search(e:string){
    if(e!=""){ 
      this.deliveries =this.mdbTable.searchLocalDataBy(e);
      this.mdbTable.setDataSource(this.list); 
    }
    else{
      this.mdbTable.setDataSource(this.list);
    }
 
  }
 change(delivery:Delivery){
   let profit = delivery.total - (delivery.price+delivery.deliveryFee)
   profit = +profit.toFixed(1);
   return profit
 }
 status(value){
   switch(value){
     case 'init':return "En attente de restaurant";
     case 'wait':return "Recherche Livreur";
     case 'accepted':return "En cours de préparation";
     case 'delivered':return "Livrée";
     case 'refused':return "Refusee par le restaurant";
     case 'cancelled':return "Annulee par le client";
     case 'onWay':return "En route";
     case 'affected':return "Affectée a un livreur";
     case 'updated':return "Maj prix";


  
   }
 }
  getDeliveryMenList(){
    this.delieveryManService.getAllDeliveryMans().subscribe(deliveryMen=>{
      this.deliveryMen=deliveryMen;
    })
  }
  getDeliveryMan(id){
    let man :DeliveryMan = this.deliveryMen.find(item=>item._id==id)
    
    return man.firstName + " ("+man.phoneNumber+")";
  }
  

  getOrder(id){
    let delivery = this.deliveries[id];
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }
    getRecipient(id){
      let delivery = this.deliveries[id];
      if(delivery.recipient){
        this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
          delivery.recipient=  recipient.firstName + "("+recipient.phoneNumber+")";
        });
      }  
    }
   getSender(id){
    let delivery = this.deliveries[id];
    if(delivery.object){
      this.senderService.getSender(delivery.sender).subscribe(sender=>{
        delivery.sender=  sender.name
      });
    } 
   }


}

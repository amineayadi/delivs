import { Component, OnInit, ViewChild } from '@angular/core';
import { Recipient } from 'src/app/_models/recipient';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { MdbTableDirective } from 'angular-bootstrap-md';
import { refCount } from 'rxjs/operators';
import { ControlPosition } from '@agm/core';
import { PromocodeService } from 'src/app/_services/promocode/promocode.service';
import { Promocode } from 'src/app/_models/promocode';

@Component({
  selector: 'app-recipient-list',
  templateUrl: './recipient-list.component.html',
  styleUrls: ['./recipient-list.component.scss']
})
export class RecipientListComponent implements OnInit {
  @ViewChild(MdbTableDirective, { static: true }) mdbTable:MdbTableDirective;
  @ViewChild('updateframe', { static: true })  updateframe;

  list:Recipient[]=[];
  p=1;
  Promocode2: Promocode[] = [];
  promocode = new Promocode();
 // promocode:Promocode= new Promocode();


  recipients:Recipient[]=[]
  constructor(private recipientService:RecipientService, private promocodeService:PromocodeService
    ) { }

  ngOnInit() {
    this.getRecipients();
    this.getPromocode()
    this.getPromocode()
  }
  getPromocode() {
    this.promocodeService.getAllPromocodes().subscribe(data => {
      console.log(data)
      console.log(this.Promocode2)
      // console.log(deliveryMen2);
      //console.log(deliveryMen)

      /* let result=   deliveryMen2.find(function(elem){
     
     
           return elem.phoneNumber== "27895661"
         }) */
      // console.log(result)
    })
  }
  openUpdateModal(promocode){
    this.promocode=promocode
    this.updateframe.show();
  }
  getRecipients(){
    this.recipientService.getAllRecipients().subscribe(data=>{
     // console.log(data)
      data.reverse()
      data.forEach(item=>this.verifystatus(item))
      this.list= data
      this.recipients=data
      this.mdbTable.setDataSource(this.list);
    })
  }
  verifystatus(recipient:Recipient){
    if(recipient.connectionInfo){
    let today = new Date().getTime() ;
    let da = new Date(recipient.connectionInfo.connectionDate).getTime()
    if(recipient.connectionInfo.connected === true && (today-da) > 300000){
      this.update(recipient)
    }
  }
  }
  getClass(recipient:any)
   { let today = new Date().getTime() ;
     let create = new Date(recipient.createdAt).getTime()
     let diff = ((today-create)/86400000).toFixed(0)
     return diff;
    
   }
  update(recipient:Recipient){
    recipient.connectionInfo.connected=false;
    this.recipientService.updateRecipient(recipient).subscribe(data=>console.log(data))
  }


  Addpromo() {
    this.promocode.isActive= true, 

    this.promocodeService.createPromocode(this.promocode).subscribe(
    );
    console.log(this.promocode.code)

  }


  search(e:string){
    if(e!=""){ 
      this.recipients =this.mdbTable.searchLocalDataBy(e);
      this.mdbTable.setDataSource(this.list); 
    }
    else{
      this.getRecipients();
    }
 
  }
}

import { Component, OnInit } from '@angular/core';
import { Sender } from './../../_models/sender';
import { ActivatedRoute, Router } from '@angular/router';
import { SenderService } from './../../_services/sender/sender.service'
import { Product } from './../../_models/product';
import { Category } from 'src/app/_models/category';
import { CategoryService } from 'src/app/_services/category/category.service';

@Component({
  selector: 'app-update-restaurant',
  templateUrl: './update-restaurant.component.html',
  styleUrls: ['./update-restaurant.component.scss']
})
export class UpdateRestaurantComponent implements OnInit {

  sender:Sender = new Sender();
  id="";
  categories:Category[]= []
  constructor(private route: ActivatedRoute, private senderService:SenderService,private router:Router
    ,private categoryService:CategoryService) { }

    ngOnInit() {
      this.getCategories();
      this.getSender();
      
      
  }
  getCategories(){
    this.categoryService.getAllcategories().subscribe(categories=>{
      this.categories=categories;
    });
  }
  readUrlParams(callback) {
    this.route.queryParams.subscribe(queryParams => {
      this.route.params.subscribe(routeParams => {
        callback(routeParams, queryParams);
      });
    });
  }
  getSender() {
    this.readUrlParams((routeParams, queryParams) => {
      this.id = routeParams.id;
      this.senderService.getSender(this.id).subscribe(sender => {this.sender = sender;
      }); 
  
    })
  }
  addProduct(){
    let id = (this.sender.menu)?this.sender.menu.length:0;
         (this.sender.menu)?'':this.sender.menu=[];
    this.sender.menu[id]=new Product();
  }
  updateSender(){
    this.senderService.updateSender(this.sender).subscribe(sender=>{
      this.router.navigate(["admin/PageRestaurants/"+this.id]);

    });
    }
}

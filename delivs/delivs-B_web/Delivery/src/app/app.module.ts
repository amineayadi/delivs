import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RecipientModule } from './recipient/recipient.module';
import { DeliveryManModule } from './delivery-man/delivery-man.module'
import { RecipientComponent } from './recipient/recipient.component';
import { DeliveryManComponent } from './delivery-man/delivery-man.component';
import { SenderModule} from './sender/sender.module';
import { SenderComponent } from './sender/sender.component';
import { AdminComponent } from './admin/admin.component';
import { AdminModule } from './admin/admin.module';
import { HttpClientModule }    from '@angular/common/http';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';


@NgModule({
  declarations: [
    AppComponent,
    DeliveryManComponent,
    RecipientComponent,
    SenderComponent,
    AdminComponent
    ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    RecipientModule,
    DeliveryManModule,
    SenderModule,
    AdminModule,
    HttpClientModule,
    MDBBootstrapModule.forRoot(),
    NgbModule
  ],
  exports:[],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

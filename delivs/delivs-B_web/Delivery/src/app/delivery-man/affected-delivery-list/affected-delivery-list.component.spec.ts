import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AffectedDeliveryListComponent } from './affected-delivery-list.component';

describe('AffectedDeliveryListComponent', () => {
  let component: AffectedDeliveryListComponent;
  let fixture: ComponentFixture<AffectedDeliveryListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AffectedDeliveryListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AffectedDeliveryListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

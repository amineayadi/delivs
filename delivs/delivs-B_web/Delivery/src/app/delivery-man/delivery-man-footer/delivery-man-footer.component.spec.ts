import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryManFooterComponent } from './delivery-man-footer.component';

describe('DeliveryManFooterComponent', () => {
  let component: DeliveryManFooterComponent;
  let fixture: ComponentFixture<DeliveryManFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryManFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryManFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

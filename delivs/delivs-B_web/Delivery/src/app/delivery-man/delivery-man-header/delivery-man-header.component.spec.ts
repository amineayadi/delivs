import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DeliveryManHeaderComponent } from './delivery-man-header.component';

describe('DeliveryManHeaderComponent', () => {
  let component: DeliveryManHeaderComponent;
  let fixture: ComponentFixture<DeliveryManHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DeliveryManHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DeliveryManHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

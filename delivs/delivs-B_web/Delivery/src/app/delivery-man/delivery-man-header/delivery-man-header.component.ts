import { Component, OnInit } from '@angular/core';
import { DeliveryManLoginService } from 'src/app/_services/deliveryMan/delivery-login.service';
import { Router } from '@angular/router';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import { ConnectionInfo } from 'src/app/_models/recipient';

@Component({
  selector: 'app-delivery-man-header',
  templateUrl: './delivery-man-header.component.html',
  styleUrls: ['./delivery-man-header.component.scss']
})
export class DeliveryManHeaderComponent implements OnInit {
  active:Boolean;
  delievryman:DeliveryMan= new DeliveryMan();
  constructor(public router: Router,private loginService:DeliveryManLoginService,private deliverymanService:DeliveryManService) { }

  ngOnInit() {
    this.deliverymanService.getDeliveryMan(this.loginService.getDeliveryMan()).subscribe(data=>{
      this.active=data.active;
      setInterval(() => {
        this.update(true) 
        },300000)
    })
  }
  update(status){
    if(this.loginService.getDeliveryMan()){
      this.deliverymanService.getDeliveryMan(this.loginService.getDeliveryMan()).subscribe(delievryman=>{
      
        if(delievryman){
          let info = new ConnectionInfo(status)
          delievryman.connectionInfo = info;
          this.deliverymanService.updateDeliveryMan(delievryman).subscribe(data=>{
          })
        }
        })
    
     
  }
   
  }
  logout(){
    this.update(false)
    this.loginService.logout();
  ;
   
  }
}

import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuardService } from '../_guards/auth-guard-deliveryMan.service';
import { DeliveryManComponent} from './delivery-man.component'
import { DeliverylistComponent } from './deliverylist/deliverylist.component';
import { AffectedDeliveryListComponent } from './affected-delivery-list/affected-delivery-list.component';
import {DeliveryManProfilComponent} from './delivery-man-profil/delivery-man-profil.component';
import { DeliveryManSigninComponent} from './delivery-man-signin/delivery-man-signin.component'
import {HistoriqueLivraisonComponent} from './historique-livraison/historique-livraison.component';
import {DeliveryInprogressComponent} from './delivery-inprogress/delivery-inprogress.component';
import { ConfirmMailComponent } from './confirmMail/confirm-mail/confirm-mail.component'

const routes: Routes = [
  {
    path: '',
    component: DeliveryManComponent,
     children: [
        {
            path: 'deliveryman/waitlist',
            component: DeliverylistComponent,
            canActivate: [AuthGuardService]
        },
        {
          path: 'deliveryman/demande',
          component: AffectedDeliveryListComponent,
          canActivate: [AuthGuardService]
      },
      {
        path: 'deliveryman/account',
        component: DeliveryManProfilComponent,
        canActivate: [AuthGuardService]
    },
    {
      path: 'deliveryman/history',
      component: HistoriqueLivraisonComponent,
      canActivate: [AuthGuardService]
  },
  {
    path:'deliveryman/inprogress',
    component: DeliveryInprogressComponent,
    canActivate: [AuthGuardService]
  }
      ]
      },
      {
        path: 'deliveryman',
        component: DeliveryManSigninComponent
    },
    {
      path: 'deliveryman/confirmation/:token/:mail',
      component: ConfirmMailComponent
  }
   


    
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DeliveryManRoutingModule { }

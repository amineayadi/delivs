import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { FormsModule, ReactiveFormsModule} from '@angular/forms'; // <-- #1 import module
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { DeliveryManRoutingModule } from './delivery-man-routing.module';
import { DeliveryManHeaderComponent } from './delivery-man-header/delivery-man-header.component';
import { DeliveryManFooterComponent } from './delivery-man-footer/delivery-man-footer.component';
import { DeliverylistComponent } from './deliverylist/deliverylist.component';
import { AffectedDeliveryListComponent } from './affected-delivery-list/affected-delivery-list.component';
import { DeliveryManSigninComponent } from './delivery-man-signin/delivery-man-signin.component';
import { DeliveryManProfilComponent } from './delivery-man-profil/delivery-man-profil.component';
import { HistoriqueLivraisonComponent } from './historique-livraison/historique-livraison.component';
import { DeliveryInprogressComponent } from './delivery-inprogress/delivery-inprogress.component';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { GoogleModule} from './../google/google.module';
import { ConfirmMailComponent } from './confirmMail/confirm-mail/confirm-mail.component'
import { SocialLoginModule, AuthServiceConfig, LoginOpt } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider,AuthService  } from "angularx-social-login";


export function socialConfigs() {    
  const config = new AuthServiceConfig(    
    [    
      {    
        id: FacebookLoginProvider.PROVIDER_ID,    
        provider: new FacebookLoginProvider("440980886545911")   
      },    
      {    
        id: GoogleLoginProvider.PROVIDER_ID,    
        provider: new GoogleLoginProvider("962001351326-h47a80h4bobqgldu55c2n55kusfn1m87.apps.googleusercontent.com")   
      }    
    ]    
  );    
  return config;    
}
@NgModule({
  declarations: [DeliveryManHeaderComponent, DeliveryManFooterComponent, DeliverylistComponent, AffectedDeliveryListComponent, DeliveryManSigninComponent, DeliveryManProfilComponent, HistoriqueLivraisonComponent, DeliveryInprogressComponent, ConfirmMailComponent],
  imports: [
    CommonModule,
    GoogleModule,
    TruncateModule,
    BrowserAnimationsModule,
    DeliveryManRoutingModule,
    FormsModule,
     ReactiveFormsModule,
    MDBBootstrapModule.forRoot()
  ],
  providers: [ 
    AuthService,    
    {    
      provide: AuthServiceConfig,    
      useFactory: socialConfigs    
    }
   ],
  exports: [DeliveryManHeaderComponent, DeliveryManFooterComponent],
})
export class DeliveryManModule { }

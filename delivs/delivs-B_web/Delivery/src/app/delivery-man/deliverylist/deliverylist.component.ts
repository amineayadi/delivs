import { Component, OnInit } from '@angular/core';
import { Delivery ,history} from 'src/app/_models/delivery';
import {DeliveryService} from '../../_services/delivery/delivery.service';
import { DeliveryManLoginService } from 'src/app/_services/deliveryMan/delivery-login.service';
import { OrderService } from 'src/app/_services/order/order.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { readmore } from './../../_services/truncate'
import { NoteService } from 'src/app/_services/note/NoteService';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import {Notification} from './../../_models/notification';

@Component({
  selector: 'app-deliverylist',
  templateUrl: './deliverylist.component.html',
  styleUrls: ['./deliverylist.component.scss']
})
export class DeliverylistComponent implements OnInit {
  Delivery:Delivery[] = [] 
  selectedId="";
  change = readmore;
  blocked:boolean=false
  limit=4;
  constructor(private DeliveryService:DeliveryService,private loginService:DeliveryManLoginService,private deliverymanService:DeliveryManService,
    private orderService:OrderService,private recipientService:RecipientService,private senderService:SenderService,private noteServcie:NoteService) { }

  ngOnInit() {

    this.getDelivery();
    this.getDeliveryMan()

  }
  getDeliveryMan(){
    let id =this.loginService.getDeliveryMan();
    this.deliverymanService.getDeliveryMan(id).subscribe(deliveryman=>{
      this.blocked=!deliveryman.available;
    })
  }
  getDelivery(){
    this.DeliveryService.getDeliveryByStatus("wait").subscribe(Delivery=>{
      this.Delivery = Delivery;
      this.Delivery.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index),this.getSender(index)})
    });
  }
  getOrder(id){
    let delivery = this. Delivery[id];
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }
    getRecipient(id){
      let delivery = this. Delivery[id];
      if(delivery.recipient){
        this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
          delivery.recipient=  recipient.firstName + " "+ recipient.lastName;
        });
      }  
    }
    getSender(id){
      let delivery = this. Delivery[id];
      if(delivery.object){
        this.senderService.getSender(delivery.sender).subscribe(sender=>{
          delivery.sender=  sender as any
        });
      }
    }
    accept(){
      this.DeliveryService.getDelivery(this.selectedId).subscribe(delivery=>{
        delivery.status=(delivery.object)?"accepted":"affected";
        delivery.statusHistory.push(new history(delivery.status));
        delivery.deliveryMan=this.loginService.getDeliveryMan();
        console.log(delivery)
  
        this.DeliveryService.updateDelivery(delivery).subscribe(updatedDelivery=>{
          if (updatedDelivery.object){
            this.sendNotif("La commande est en cours de preparation","/recipient/orders",updatedDelivery.recipient)
            this.sendNotif("Le livreur est en route pour la commande","/restaurant/orders/inprogress",updatedDelivery.sender)
          }
          else {
            this.sendNotif("Un livreur a accepté votre commande ","/recipient/orders",updatedDelivery.recipient)
          }
       this.getDelivery()
         
        }) 
      }) 
    }
  
    sendNotif(message,route,destination){
      let notif2 = new Notification("Nouvelle commande",message,route,destination);
          this.DeliveryService.sendNotification(notif2).subscribe(data=>{
          })
    }


    
  }


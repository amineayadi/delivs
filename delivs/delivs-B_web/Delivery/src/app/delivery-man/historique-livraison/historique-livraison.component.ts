import { Component, OnInit, ViewChild } from '@angular/core';
import { Delivery } from 'src/app/_models/delivery';
import {DeliveryService} from '../../_services/delivery/delivery.service';
import { OrderService } from 'src/app/_services/order/order.service';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { readmore } from './../../_services/truncate'
import { DeliveryManLoginService } from 'src/app/_services/deliveryMan/delivery-login.service';

@Component({
  selector: 'app-historique-livraison',
  templateUrl: './historique-livraison.component.html',
  styleUrls: ['./historique-livraison.component.scss']
})
export class HistoriqueLivraisonComponent implements OnInit {

  change = readmore;
  Delivery:Delivery[] = []
  status=[
    {id:"delivered",text:"Livrée"}, 
    { id:"cancelled",text:"Annulée"}
  
  ]
  constructor(private DeliveryService:DeliveryService,private loginService:DeliveryManLoginService,
    private orderService:OrderService,private recipientService:RecipientService,private senderService:SenderService) { }

  ngOnInit() {
    this.getDelivery();
  }
  find(status){
    let item = this.status.find(item=>item.id===status)
    return item.text
  }
  getDelivery(){
    this.DeliveryService.getDeliveryByDelievryManAndStatus(this.loginService.getDeliveryMan(),"delivered,cancelled").subscribe(Delivery=>{
      this.Delivery = Delivery;
      this.Delivery.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index),this.getSender(index)})
    });
  }
  getSender(id){
    let delivery = this. Delivery[id];
    if(delivery.object){
      this.senderService.getSender(delivery.sender).subscribe(sender=>{
        delivery.sender=  sender as any
      });
    }
  }
  getOrder(id){
    let delivery = this. Delivery[id];
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }
    getRecipient(id){
      let delivery = this. Delivery[id];
      if(delivery.recipient){
        this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
          delivery.recipient=  recipient.firstName + " "+ recipient.lastName;
        });
      }  
    }

 

  }

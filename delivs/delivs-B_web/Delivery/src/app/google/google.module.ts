import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GoogleComponent } from './google/google.component';
import { AgmCoreModule } from '@agm/core';

@NgModule({
  declarations: [GoogleComponent],
  imports: [
    CommonModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyA7WoC8kDd-46mQXoaBj2NTRMDZ-xEAkp8',
      libraries: ['places','geometry']
    })
  ],
  exports: [
    GoogleComponent
],
providers: [GoogleComponent],
})
export class GoogleModule { }
//AIzaSyB_x0SHuUEolw4wmj-u3P93Eco0AmZFtcA

//AIzaSyA7WoC8kDd-46mQXoaBj2NTRMDZ-xEAkp8 last one 
import { Component, OnInit, ViewChild, NgZone, ElementRef, Input, Output, EventEmitter } from '@angular/core';
import { MapsAPILoader } from '@agm/core';
import { MouseEvent as AGMMouseEvent } from '@agm/core'
import { DeliveryMan } from 'src/app/_models/deliveryMan';
import { isArray } from 'util';
import { Delivery } from 'src/app/_models/delivery';
import { GoogleService } from 'src/app/_services/google.service';

@Component({
  selector: 'app-google',
  templateUrl: './google.component.html',
  styleUrls: ['./google.component.scss']
})
export class GoogleComponent implements OnInit {

  latitude: number;
  longitude: number;
  zoom: number;
  address: string;
  private geoCoder; 
  @Input() parent: any;
  @Input() value: any;
  @Input() type: boolean;
  markers: marker[] =[];
  previous;
  points=[]
  infowindow
  @Output() messageEvent = new EventEmitter<string>();
  constructor( private mapsAPILoader: MapsAPILoader,
    private ngZone: NgZone,private googleService:GoogleService) { }

  ngOnInit() {
    this.load()
   
  }
  load(){
    this.mapsAPILoader.load().then(() => {
      
      this.setCurrentLocation();
      this.geoCoder = new google.maps.Geocoder;
      if (this.parent.searchElementRef != undefined){
      let autocomplete = new google.maps.places.Autocomplete(this.parent.searchElementRef.nativeElement);
      autocomplete.addListener("place_changed", () => {
        this.ngZone.run(() => {
          //get the place result
          let place: google.maps.places.PlaceResult = autocomplete.getPlace();
  
          //verify result
          if (place.geometry === undefined || place.geometry === null) {
            return;
          }
  
          //set latitude, longitude and zoom
          this.latitude = place.geometry.location.lat();
          this.longitude = place.geometry.location.lng();
          this.zoom = 12;
          this.getAddress(this.latitude, this.longitude);
        });
      });
    }
 if(!this.type){
   this.addListener();
}
      else {
      this.getmarkers();
      }
    });
  }
  addListener(){

  }
  mapClicked($event: AGMMouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }
  public  setCurrentLocation() {
 
    if ('geolocation' in navigator) {
      navigator.geolocation.getCurrentPosition((position) => {
        this.latitude = position.coords.latitude;
        this.longitude = position.coords.longitude;
        this.zoom=12;
        if(!this.type){  this.getAddress(this.latitude, this.longitude);}
      },(error)=> {
        this.googleService.getLocation().subscribe(coords=>{
          this.latitude = coords.location.lat
          this.longitude = coords.location.lng
          this.zoom=12;
          if(!this.type){  this.getAddress(this.latitude, this.longitude);}
        }) 
      });
    }
  }
 
 
  markerDragEnd($event: AGMMouseEvent) {
    this.latitude = $event.coords.lat;
    this.longitude = $event.coords.lng;
    this.getAddress(this.latitude, this.longitude);
  }
 
  getAddress(latitude, longitude) {
    
    this.geoCoder.geocode({ 'location': { lat: latitude, lng: longitude } }, (results, status) => {
      if (status === 'OK') {
        if (results[0]) {
        
          this.zoom = 12;
          this.address = results[0].formatted_address;
          this.parent.searchElementRef.nativeElement.value=this.address;
        } else {
          window.alert('No results found');
        }
      } else {
        window.alert('Geocoder failed due to: ' + status);
      }
 
    });
  }
  clickedMarker(infowindow) {
    if (this.previous) {
        this.previous.close();
    }
    this.previous = infowindow;
 }
 sendMessage(id) {
  this.messageEvent.emit(id)
}
  getmarkers(){

    let values : any;
    /* affect  Deliveryman  */
    if (isArray(this.value)){
    values = this.value as DeliveryMan[]
    values.forEach((item,index)=>{
      this.markers.push({
        item:item,
        lat: item.coordinates.latitude,
        lng: item.coordinates.longitude,
        label:'L'+(index+1) 
      })

    })
  }
  /* Delivery Info */
  else {
   
    values = this.value as Delivery;
    this.markers.push({
      lat: values.startingcoordinates.latitude *1,
      lng: values.startingcoordinates.longitude *1,
      label:"De"
    })
    this.markers.push({
      lat: values.arrivalcoordinates.latitude *1,
      lng: values.arrivalcoordinates.longitude *1,
      label:"A"
    })
  }
  }
 static calculateDistance(pointa,pointb):number {
    const A = new google.maps.LatLng(pointa.latitude as number,pointa.longitude as number);
    const B = new google.maps.LatLng(pointb.latitude as number,pointb.longitude as number);
    const distance = google.maps.geometry.spherical.computeDistanceBetween(A, B);
    return distance*0.001
  }

}
interface marker {
  item?:any;
	lat: number;
  lng: number;
  label?:string

}
import { Component, OnInit } from '@angular/core';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-mail-confirmation',
  templateUrl: './mail-confirmation.component.html',
  styleUrls: ['./mail-confirmation.component.scss']
})
export class MailConfirmationComponent implements OnInit {
  expired=false;

  constructor(private route: ActivatedRoute,private recipientServcie:RecipientService,private location: Location) { }

  ngOnInit() {
    this.getToken();
    this.location.replaceState(Location.joinWithSlash('recipient','confirmation'))

  }
  readUrlParams(callback) {
    this.route.queryParams.subscribe(queryParams => {
      this.route.params.subscribe(routeParams => {
        callback(routeParams, queryParams);
      });
    });
  }
  getToken(){
    this.readUrlParams((routeParams, queryParams) => {
      let token = routeParams.token;
      let mail = routeParams.mail;
      this.recipientServcie.confirmRecipientMail(token,mail).subscribe(data=>{
        if(!data){this.expired=true;}
            })
  
    });
  }
}

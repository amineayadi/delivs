import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipientCommandePersoComponent } from './recipient-commande-perso.component';

describe('RecipientCommandePersoComponent', () => {
  let component: RecipientCommandePersoComponent;
  let fixture: ComponentFixture<RecipientCommandePersoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipientCommandePersoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipientCommandePersoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { Sender } from 'src/app/_models/sender';

@Component({
  selector: 'app-recipient-footer',
  templateUrl: './recipient-footer.component.html',
  styleUrls: ['./recipient-footer.component.scss']
})
export class RecipientFooterComponent implements OnInit {
  senders:Sender[]=[];
  constructor(private senderService:SenderService) { }

  ngOnInit() {
  this.getSenders()
  }
  getSenders(){
    this.senderService.getAllSenders().subscribe(senders=>{
      this.senders=senders.slice(0,3);
    }) }

}

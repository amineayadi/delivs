import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipientHistoriqueComponent } from './recipient-historique.component';

describe('RecipientHistoriqueComponent', () => {
  let component: RecipientHistoriqueComponent;
  let fixture: ComponentFixture<RecipientHistoriqueComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipientHistoriqueComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipientHistoriqueComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

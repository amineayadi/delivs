import { Component, OnInit } from '@angular/core';
import { SenderService } from '../../_services/sender/sender.service';
import { Delivery } from '../../_models/delivery';
import { DeliveryService } from '../../_services/delivery/delivery.service';
import { RecipientService } from '../../_services/recipient/recipient.service';
import { OrderService } from '../../_services/order/order.service';
import {NgbRatingConfig} from '@ng-bootstrap/ng-bootstrap';
import { Note } from 'src/app/_models/note';
import { RecipientLoginService } from 'src/app/_services/recipient/recipient-login.service';
import { NoteService } from 'src/app/_services/note/NoteService';
import { readmore } from './../../_services/truncate'

import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';


@Component({
  selector: 'app-recipient-historique',
  templateUrl: './recipient-historique.component.html',
  styleUrls: ['./recipient-historique.component.scss'],
  providers: [NgbRatingConfig]
})
export class RecipientHistoriqueComponent implements OnInit {
id;
deliveries:Delivery[]=[];
sender;
rate=0;
selectedId;
selectedDelivery:Delivery=new Delivery();
comment;
isdeliveryman=false;
p=1;
status=[
  { id:"refused",val:"Commande Refusée"},
  { id:"cancelled",val:"Commande Annulée"},
 { id:"delivered",val:"Commande Livrée"},
]
change = readmore;  
  note = new Note()
  constructor(private recipientService:RecipientService, private deliveryService:DeliveryService, private senderService:SenderService, private orderService:OrderService,
    private config: NgbRatingConfig,private loginService:RecipientLoginService,private noteService:NoteService,private deliverymanService:DeliveryManService)
    {config.max=5; }

  ngOnInit() {
    this.getDeliveryByRecipientAndStatus();
  
  }
  find(status){
    //this.status=this.statusALL;
    
        let index = this.status.findIndex(item=>(item.id===status))
    
        return this.status[index].val;
      }
  getDeliveryByRecipientAndStatus () {
    this.deliveryService.getDeliveryByRecipientAndStatus("delivered,refused,cancelled")
    .subscribe(deliveries=>{this.deliveries=deliveries
      this.deliveries.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index),this.getSender(index)})
    })


  }

  save(){
    if(this.selectedDelivery.object===null){this.isdeliveryman=true}
    this.note.delivery=this.selectedDelivery._id;
    this.note.accountType=(this.isdeliveryman)?"deliveryman":"sender";
    this.note.accountId=(this.isdeliveryman)?this.selectedDelivery.deliveryMan:this.sender;
    this.note.recipient=this.loginService.getRecipient()
    this.note.note=this.rate;
    this.note.comment=this.comment;
    this.note.dateNote = new Date().toJSON() as any 
     this.noteService.createNote(this.note).subscribe(newnote=>{
      this.blockDeliveryMan(newnote.deliveryman)
      this.updateDelivery(newnote.delivery)
      this.rate=0;
      this.comment="";
    }) 
  }
blockDeliveryMan(id){
this.noteService.getNotesAverageByDeliveryman(id).subscribe(ave=>{
  if(ave < 4){this.updateDeliveryMan(id)}
})
}
updateDeliveryMan(id){
  this.deliverymanService.getDeliveryMan(id).subscribe(deliveryman=>{
    deliveryman.available=false;
    this.deliverymanService.updateDeliveryMan(deliveryman).subscribe(updatedDeliveryMan=>{
    })
  })
   }
 updateDelivery(id){
this.deliveryService.getDelivery(id).subscribe(delivery=>{
  delivery.reviewed=true;
  this.deliveryService.updateDelivery(delivery).subscribe(updatedDelivery=>{
    this.getDeliveryByRecipientAndStatus();
  })
})
 }
  getOrder(id){
    let delivery = this.deliveries[id];
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }

  getRecipient(id){
    let delivery = this.deliveries[id];
    if(delivery.recipient){
      this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
        delivery.recipient=  recipient.firstName + " "+ recipient.lastName;
      });
    }  
  }
  getSender(id){
    let delivery = this.deliveries[id];
    if(delivery.object){
      this.sender=delivery.sender;
      this.senderService.getSender(delivery.sender).subscribe(sender=>{
        delivery.sender=  sender.name;
      });
    }  
  }




}

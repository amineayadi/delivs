import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipientMenuRestoComponent } from './recipient-menu-resto.component';

describe('RecipientMenuRestoComponent', () => {
  let component: RecipientMenuRestoComponent;
  let fixture: ComponentFixture<RecipientMenuRestoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipientMenuRestoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipientMenuRestoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

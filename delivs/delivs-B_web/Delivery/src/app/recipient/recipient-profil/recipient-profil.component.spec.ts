import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RecipientProfilComponent } from './recipient-profil.component';

describe('RecipientProfilComponent', () => {
  let component: RecipientProfilComponent;
  let fixture: ComponentFixture<RecipientProfilComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RecipientProfilComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RecipientProfilComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

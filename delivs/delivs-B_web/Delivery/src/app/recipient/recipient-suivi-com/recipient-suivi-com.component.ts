import { Component, OnInit } from '@angular/core';
import { SenderService } from '../../_services/sender/sender.service';
import { Delivery ,history} from '../../_models/delivery';
import { DeliveryService } from '../../_services/delivery/delivery.service';
import { SenderLoginService } from '../../_services/sender/sender-login.service';
import { RecipientService } from '../../_services/recipient/recipient.service';
import { OrderService } from '../../_services/order/order.service';
import { readmore } from './../../_services/truncate'
import {Notification} from './../../_models/notification';


@Component({
  selector: 'app-recipient-suivi-com',
  templateUrl: './recipient-suivi-com.component.html',
  styleUrls: ['./recipient-suivi-com.component.scss']
})
export class RecipientSuiviComComponent implements OnInit {
  deliveries:Delivery[]=[];
  selectedId="";
  change = readmore;
  p=1;
  status=[
    {id:"init",val:"En attente Restaurant  "},
    {id:"affected",val:"Affectée a un livreur "},
    { id:"refused",val:"Commande Refusée"},
    { id:"cancelled",val:"Commande Annulée"},
    { id:"wait",val:"Recherche livreur"},
    { id:"accepted",val:"En cours de préparation"},
    { id:"onWay",val:"Commande en route"},
   { id:"delivered",val:"Commande Livrée"},
   { id:"updated",val:"Confirmez nouveau prix"}
  ]
  constructor(private orderService:OrderService, private recipientService:RecipientService, private deliveryService:DeliveryService, private senderService:SenderService, private loginService:SenderLoginService) { }

  ngOnInit() {
    this.getDeliveryByRecipient()

  }
  cancel(id){
this.deliveryService.getDelivery(id).subscribe(De=>{
De.status="cancelled"
De.statusHistory.push(new history(De.status))
 this.deliveryService.updateDelivery(De).subscribe(res=>{
   this.getDeliveryByRecipient();
})  
})
  }
  find(status){
    //this.status=this.statusALL;
    
        let index = this.status.findIndex(item=>(item.id===status))
    
        return this.status[index].val;
      }
  getDeliveryByRecipient () {
    this.deliveryService.getDeliveryByRecipientAndStatus("init,wait,accepted,onWay,updated,cancelled")
    .subscribe(deliveries=>{this.deliveries=deliveries
      this.deliveries.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index),this.getSender(index)})
    })


  }


  getOrder(id){
    let delivery=this.deliveries[id];

    
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }

  getRecipient(id){
    let delivery=this.deliveries[id];

    if(delivery.recipient){
      this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
        delivery.recipient=  recipient.firstName + " "+ recipient.lastName;
      });
    }  
  }
  getSender(id){
    let delivery=this.deliveries[id];
    if(delivery.object){
      this.senderService.getSender(delivery.sender).subscribe(sender=>{
        delivery.sender=  sender as any;
      });
    }  
  }
  confirm(status) {
    this.deliveryService.getDelivery(this.selectedId).subscribe(D=>{
      D.status=status;
      D.statusHistory.push(new history(D.status))
      let notif = null;
      if(status==='accepted'){
        notif = new Notification("Nouvelle livraison","Le client a confirmé le nouveau prix","/inProgress",D.deliveryMan);
      }
      else {
        notif = new Notification("Nouvelle livraison","Le client a annulé la commande suite au changement du prix","/inProgress",D.deliveryMan);
      }
      this.deliveryService.updateDelivery(D).subscribe(updatedDelivery=>{
       this.getDeliveryByRecipient();  
   this.deliveryService.sendNotification(notif).subscribe(data=>{})
     }) 
    });
 
  
   
  }


}

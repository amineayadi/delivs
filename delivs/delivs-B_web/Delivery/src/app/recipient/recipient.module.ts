import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { HttpClientModule }    from '@angular/common/http';
import { RecipientRoutingModule } from './recipient-routing.module';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { RecipientFooterComponent } from './recipient-footer/recipient-footer.component';
import { RecipientHeaderComponent } from './recipient-header/recipient-header.component';
import { RecipientListRestoComponent } from './recipient-list-resto/recipient-list-resto.component';
import { RecipientSigninComponent } from './recipient-signin/recipient-signin.component';
import { FormsModule ,ReactiveFormsModule} from '@angular/forms';
import { RecipientMenuRestoComponent } from './recipient-menu-resto/recipient-menu-resto.component';
import { CookieService } from 'ngx-cookie-service';
import { RecipientProfilComponent } from './recipient-profil/recipient-profil.component';
import { RecipientCommandePersoComponent } from './recipient-commande-perso/recipient-commande-perso.component';
import { RecipientHistoriqueComponent } from './recipient-historique/recipient-historique.component';
import { RecipientSuiviComComponent } from './recipient-suivi-com/recipient-suivi-com.component';
import {NgxPaginationModule} from 'ngx-pagination';
import {PasswordValidatorDirective} from './../_models/custom-validators';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { NotesComponent } from './notes/notes.component';
import { GoogleModule} from './../google/google.module';
import { MailConfirmationComponent } from './mail-confirmation/mail-confirmation.component'
import { SocialLoginModule, AuthServiceConfig, LoginOpt } from "angularx-social-login";
import { GoogleLoginProvider, FacebookLoginProvider,AuthService  } from "angularx-social-login";
import { DelivsacceuilComponent } from './delivsacceuil/delivsacceuil.component';
import { ConfidentialityComponent } from './confidentiality/confidentiality.component';
export function socialConfigs() {    
  const config = new AuthServiceConfig(    
    [    
      {    
        id: FacebookLoginProvider.PROVIDER_ID,    
        provider: new FacebookLoginProvider("440980886545911")   
      },    
      {    
        id: GoogleLoginProvider.PROVIDER_ID,    
        provider: new GoogleLoginProvider("962001351326-h47a80h4bobqgldu55c2n55kusfn1m87.apps.googleusercontent.com")   
      }    
    ]    
  );    
  return config;    
}
@NgModule({
  declarations: [PasswordValidatorDirective,RecipientFooterComponent, RecipientHeaderComponent, RecipientListRestoComponent, RecipientSigninComponent, RecipientMenuRestoComponent, RecipientProfilComponent, RecipientCommandePersoComponent, RecipientHistoriqueComponent, RecipientSuiviComComponent, NotesComponent, MailConfirmationComponent, DelivsacceuilComponent, ConfidentialityComponent],
  imports: [
    CommonModule,
    BrowserAnimationsModule,
    GoogleModule,
    NgxPaginationModule,
    RecipientRoutingModule,
    MDBBootstrapModule.forRoot(),
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    NgbModule
 
  ],
  providers: [ CookieService ,AuthService,    
    {    
      provide: AuthServiceConfig,    
      useFactory: socialConfigs    
    }],
  exports: [RecipientFooterComponent, RecipientHeaderComponent, RecipientListRestoComponent],
})
export class RecipientModule { }

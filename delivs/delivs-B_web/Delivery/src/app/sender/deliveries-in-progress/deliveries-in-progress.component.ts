import { Component, OnInit, ViewChild } from '@angular/core';
import { Delivery } from 'src/app/_models/delivery';
import { DeliveryService } from 'src/app/_services/delivery/delivery.service';
import { OrderService } from 'src/app/_services/order/order.service';
import { RecipientService } from 'src/app/_services/recipient/recipient.service';
import { DeliveryManService } from 'src/app/_services/deliveryMan/delivery-man.service';
import { DeliveryMan } from 'src/app/_models/deliveryMan';
@Component({
  selector: 'app-deliveries-in-progress',
  templateUrl: './deliveries-in-progress.component.html',
  styleUrls: ['./deliveries-in-progress.component.scss']
})
export class DeliveriesInProgressComponent implements OnInit {
 
  deliveries:Delivery[]=[];
  deliveryMen:DeliveryMan[]=[]
  constructor(private deliveryService:DeliveryService,private orderService:OrderService,private recipientService:RecipientService,
  ) { }

  ngOnInit() {
    this.getDeliveryList();
  }
  getDeliveryList(){
    this.deliveryService.getDeliveryBySenderAndStatus('accepted').subscribe(deliveries => {
      
      this.deliveries=deliveries;
      this.deliveries.forEach((item,index )=>{this.getOrder(index),this.getRecipient(index)})
    })
  }

  getOrder(id){
    let delivery = this.deliveries[id];
    if(delivery.object){
      this.orderService.getOrder(delivery.object).subscribe(order=>{
        delivery.description=  order.orderItems as any
      });
    }
  }
    getRecipient(id){
      let delivery = this.deliveries[id];
      if(delivery.recipient){
        this.recipientService.getRecipient(delivery.recipient).subscribe(recipient=>{
          delivery.recipient=  recipient.firstName + " "+ recipient.lastName;
        });
      }  
    }

     
    
   
  

}



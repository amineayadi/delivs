import { Component, OnInit } from '@angular/core';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';

@Component({
  selector: 'app-mail-confirmation',
  templateUrl: './mail-confirmation.component.html',
  styleUrls: ['./mail-confirmation.component.scss']
})
export class MailConfirmationComponent implements OnInit {
  expired=false;

  constructor(private route: ActivatedRoute,private senderService:SenderService,private location: Location) { }

  ngOnInit() {
    this.getToken()
    this.location.replaceState(Location.joinWithSlash('restaurant','confirmation'))

  }
  readUrlParams(callback) {
    this.route.queryParams.subscribe(queryParams => {
      this.route.params.subscribe(routeParams => {
        callback(routeParams, queryParams);
      });
    });
  }
  getToken(){
    this.readUrlParams((routeParams, queryParams) => {
      let token = routeParams.token;
      let mail = routeParams.mail;
      this.senderService.confirmSenderMail(token,mail).subscribe(data=>{
        if(!data){this.expired=true;}
            })
  
    });
  }
}

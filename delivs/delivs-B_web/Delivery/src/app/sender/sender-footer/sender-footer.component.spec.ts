import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SenderFooterComponent } from './sender-footer.component';

describe('SenderFooterComponent', () => {
  let component: SenderFooterComponent;
  let fixture: ComponentFixture<SenderFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SenderFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SenderFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Sender } from 'src/app/_models/sender';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';
import { Router } from '@angular/router';
@Component({
  selector: 'app-sender-header',
  templateUrl: './sender-header.component.html',
  styleUrls: ['./sender-header.component.scss']
})
export class SenderHeaderComponent implements OnInit {


  constructor(public router: Router,private loginService:SenderLoginService) { }

  ngOnInit() {
   
  }
  logout(){
    this.loginService.logout();
  }

}

import { Component, OnInit, ViewChild, ElementRef, HostListener } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Sender } from 'src/app/_models/sender';
import { SenderService } from 'src/app/_services/sender/sender.service';
import { SenderLoginService } from 'src/app/_services/sender/sender-login.service';
import { passwordValidator } from 'src/app/_models/custom-validators';
import { GoogleComponent } from 'src/app/google/google/google.component';
import { DeliveryFeeService } from 'src/app/_services/deliveryFee/delivery-fee.service';
import { SmsService } from 'src/app/_services/sms/sms.service';

@Component({
  selector: 'app-sender-signin',
  templateUrl: './sender-signin.component.html',
  styleUrls: ['./sender-signin.component.scss']
})
export class SenderSigninComponent implements OnInit {
  @ViewChild('frame', { static: true }) public formModal;
  @ViewChild('search', { static: true }) public searchElementRef: ElementRef;
  @ViewChild(GoogleComponent, { static: true }) child: GoogleComponent;
  @ViewChild('senderModal', { static: true }) senderModal: any;
  @ViewChild('modalsender', { static: true }) modalsender: any;

  fieldTextType: boolean;
  fieldTextType1: boolean;
  fieldTextType2: boolean;

  signInForm: FormGroup;
  signUpForm: FormGroup;
  /*
  0 : no error
  1:signin mail or password invalid
  2:signup mail exists in database
  3:signup mail invalid
  4:signin account not verified
  5:signup error
  6:signin success
  */
  forgotPassword = 0;
  forgot = 0;
  Sender2: Sender[] = [];
  Sender: Sender = new Sender();
  passimput:string;

  message = 0;
  new: boolean = false;
  selectedFiles: FileList;
  newSender = new Sender();
  newSender1 = new Sender();
  verif = false;
  code: string;

  checkbox = false;
  public innerWidth: any;
  mail = "";
  constructor(private smsService: SmsService, private senderService: SenderService, private loginService: SenderLoginService, private deliveryFeeService: DeliveryFeeService) { }

  ngOnInit() {
    this.innerWidth = window.innerWidth;
    this.signInFormBuild();
    this.signUpFormBuild();
    this.getSenders()
  }


  getSenders() {
    this.senderService.getAllSenders().subscribe(data => {
      this.Sender2 = data
      console.log(this.Sender2)
      // console.log(deliveryMen2);
      //console.log(deliveryMen)

      /* let result=   deliveryMen2.find(function(elem){
     
     
           return elem.phoneNumber== "27895661"
         }) */
      // console.log(result)
    })
  }

  //Switching method for the password hide and show
  toggleFieldTextType() {
    this.fieldTextType = !this.fieldTextType;
  }
  toggleFieldTextType1() {
    this.fieldTextType1 = !this.fieldTextType1;
  }
  toggleFieldTextType2() {
    this.fieldTextType2 = !this.fieldTextType2;
  }
  generatePassword() {
    this.senderService.getSenderByMail(this.mail).subscribe(delievryman => {
      let sender = new Sender(this.signInForm.value);
      const found = this.Sender2.find(item => item.email === sender.email)
      console.log(found.email)
      if (!found) {
        this.message === 1
      } else {
        let password = Math.random().toString(36).substr(2, 8);
        found.password = password;

        this.senderService.updateSender(found).subscribe(data => {
          this.smsService.checkSms(data.phoneNumber, "Votre nouveau mot de passe est  " + password).subscribe(res => {

            this.modalsender.show()
            this.signUpFormBuild();
            console.log(found)
          })
        },

        )

      }


      /*
      
      this.deliverManService.getAllDeliveryMans().subscribe(allDelivs => {
        //console.log(allDelivs)
  
        
        //console.log(this.deliveryMen3.phoneNumber)
        if (!this.deliveryMen3) {
          this.forgotPassword = 1;
        }
        else {
          let password = Math.random().toString(36).substr(2, 8);
          this.deliveryMen3.password = password;
          this.deliverManService.updateDeliveryMan(this.deliveryMen3).subscribe(deliveryMen3 => {
            this.smsService.checkSms(deliveryMen3.phoneNumber, "le code d'activation de votre compte est  " + this.deliveryMen3.password).subscribe(res => console.log(res))
            this.Modal.show()
          })
  
        }
  */
    })


  }
  verification() {
    let sender = new Sender(this.signInForm.value);


    const found = this.Sender2.find(item => item.email === sender.email)
    if (found.password === this.passimput) {
      this.verif = false
      this.loginService.login(found._id, true)

    }
    else {
      this.verif = true
      this.forgot = 1

    }
  }
  @HostListener('window:resize', ['$event'])
  onResize(event) {
    this.innerWidth = window.innerWidth;
  }
  signInFormBuild() {
    this.signInForm = new FormGroup({
      email: new FormControl('', [Validators.email, Validators.required]),
      password: new FormControl('', Validators.required)
    });
  }
  signUpFormBuild() {
    this.signUpForm = new FormGroup({
      name: new FormControl('', Validators.required),
      lastName: new FormControl(''),
      email: new FormControl('', [Validators.email, Validators.required]),
      address: new FormControl('', Validators.required),
      password: new FormControl('', [Validators.required, Validators.minLength(5), passwordValidator()]),
      passwordConfirmation: new FormControl('', [Validators.required, Validators.minLength(5), passwordValidator()]),
      type: new FormControl(true, Validators.required),
      // banner: new FormControl('', [Validators.required]),
      phoneNumber: new FormControl('', Validators.required)
    });
    this.newSender.coordinates = null;
  }
  get getSignInFormEmail() {
    return this.signInForm.get('email');
  }
  get getSignInFormPassword() {
    return this.signInForm.get('password');
  }
  get getSignupFormName() {
    return this.signUpForm.get('name');
  }

  get getSignupFormEmail() {
    return this.signUpForm.get('email');
  }

  get getSignupFormPassword() {
    return this.signUpForm.get('password');
  }
  get getSignupFormType() {
    return this.signUpForm.get('type');
  }
  get getSignupFormBanner() {
    return this.signUpForm.get('banner');
  }
  get getSignupFormLastName() {
    return this.signUpForm.get('lastName');
  }



  get getSignupFormphoneNumber() {
    return this.signUpForm.get('phoneNumber');
  }
  get getSignupFormAddress() {
    return this.signUpForm.get('address');
  }
  get getSignupFormPasswordC() {
    return this.signUpForm.get('passwordConfirmation');
  }
  onPaste(event: ClipboardEvent) {
    event.preventDefault()
  }
  checkPassword(type) {
    this.message = 0;
    if (type === 0 && this.signUpForm.value.passwordConfirmation != "" && this.signUpForm.value.password !== this.signUpForm.value.passwordConfirmation) {
      this.message = 7;
    }
    if (type === 1 && this.signUpForm.value.password != "" && this.signUpForm.value.password !== this.signUpForm.value.passwordConfirmation) {
      this.message = 7;
    }

  }
  selectFile(event) {
    this.selectedFiles = event.target.files;
    this.signUpForm.get('banner').setValue(this.selectedFiles.item(0).name)
  }
  signin() {
    let sender = new Sender(this.signInForm.value);

    this.senderService.getLoginSender(sender.email, sender.password).subscribe(newsender => {
      if (newsender) {
        this.message = 0;
        (newsender.verified) ? this.loginService.login(newsender._id, true) : this.message = 4;;
      } else {
        this.message = 1;
      }
    })
  }
  check() {
    if (this.getSignupFormEmail.valid) {
      this.deliveryFeeService.getOneDeliveryFee().subscribe(data => {
        let value = data as any
        this.senderService.checkMail(value.key, this.signUpForm.value.email).subscribe(data => {
          if (data.status === 200) {
            this.message = 0;
          }
          else {
            this.message = 3
          }
        })
      })
    }
  }



  verified() {

    if (this.newSender1.token === this.code) {
      this.verif = false
      this.loginService.login(this.newSender1._id, true)
    }
    else {
      this.verif = true
    }
  }
  signup() {
    delete this.signUpForm.value.passwordConfirmation;
    let sender = new Sender(this.signUpForm.value);
    sender.coordinates = this.newSender.coordinates;
    sender.banner = null;
    (sender.type) ? sender.type = "0" : sender.type = "1";
    this.senderService.createSender(sender).subscribe(newsender => {
      this.newSender1 = newsender

      this.message = 0
      if (this.newSender1 === true as any) {
        this.message = 2;
      } else if (this.newSender1 != null) {
        this.smsService.checkSms(newsender.phoneNumber, "le code d'activation de votre compte est  " + this.newSender1.token).subscribe(res => console.log(res))
        this.signUpFormBuild();
        this.senderModal.show()
        //this.message=0;
        //newsender.coordinates = this.newSender.coordinates
        //this.new=false;
        //this.message=6;
        //this.signUpFormBuild();


      }
      /*    */
    });
    /* this.senderService.uploadImage(this.selectedFiles.item(0)).subscribe(data=>{
      
    },err=>{console.log(err);this.message=5;}   ); */

  }
  setAddress() {
    this.newSender.coordinates = { latitude: this.child.latitude, longitude: this.child.longitude };
    console.log(this.newSender.coordinates)
    this.newSender.address = this.child.address
    this.signUpForm.controls['address'].setValue(this.newSender.address);
  }


}
